<?php include 'sendtriallesson.php'; ?>



<!DOCTYPE html>

<html lang="en">

<head>

	<title>NILS Online - About Nils</title>

  <?php include 'header.php'?>

</head>

<body data-spy="scroll" data-target=".navbar-collapse" data-offset="50">

     <!-- PRE LOADER -->

     <section class="preloader">

          <div class="spinner">



               <span class="spinner-rotate"></span>

               

          </div>

     </section>



     <!-- Navbar -->

     <?php include 'navbar.php';?>



     <!-- STICKY SOCIAL -->

     <?php include 'sticky_social.php';?>

     

     <section id="about">

     	       <div class="container">

               <div class="row mb-5">



                    <div class="col-md-6 col-sm-12">

                         <div class="about-info">

                              <h2>TESDAってなに？</h2>

                              <hr>

                              <p class="text-tesda">TESDAはTechnicalEducation and Skills Development Authorityの略で、フィリピンの技術教育およびスキル開発（TESD）の管理と監督を任務とする政府機関です。</p><br>

                              <p class="text-tesda">彼らの使命の1つは、中堅レベルの人材育成に関与する機関の認定システムを開発することです。</p>

                         <p class="text-tesda"> JICCの傘下にあるNILSは文部科学省に相当する機関として認定されています。 NILSは2012年からTESDAに登録されています。</p>

                         </div>

                    </div>



                    <div class="col-md-offset-1 col-md-4 col-sm-12">

                         <div class="entry-form">

                              <img src="images/tesda.png" width="260">

                         </div>

                    </div>



               </div>

               <div class="row">

                    <div class="col-md-3"></div>

                    <div class="col-md-6">

                         <div class="embed-responsive embed-responsive-16by9 mt-5">

                           <!-- <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/zpOULjyy-n8?rel=0" allowfullscreen></iframe> -->

                           <iframe width="1280" height="720" src="https://www.youtube.com/embed/pfx2MqV3I-w" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

                         </div>

                         <h3 class="text-center"><strong>Watch a sample of our Lesson</strong></h3>

                    </div>

                    <div class="col-md-3"></div>

               </div>

               <div class="row">

                    <div class="container">

                         <h2>NILSってなに？</h2>

                         <hr>

                         <p class="text-nils">NILSはNewtypeInternational LanguageSchoolの略です。 2011年にセブ市のITパークに設立された英語学校です。 NILSは、学生に英語のレッスンを提供するための質の高いサービスを提供しています。 将来世界中で活躍するための人材育成を目指しており、それを強化することが最大のポイントです。</p>

                                             <p class="text-nils">2018年には、学生へのオンライン英会話の提供を開始しました。 2021年4月よりそのノウハウを活かし、JICCに引き継がれ、現在でも、多くの個人や企業に質の高いクラスを提供しています。</p>

                    </div>

               </div>

               <div class="row">

               		<div class="container">

               			<h2 class="text-center">まずは、高品質なレッスンを体験してください。</h2>

               			<p class="about-text">まずは、無料で体験してみてください。<br>

							担当者様だけではなく、受講予定の全ての生徒様を<br>

							無制限で無料体験レッスンにご招待いたします。

（マンツーマン・グループをご選択いただけます。）</p>

						<div class="row">

						<div class="col-md-6">

							<img src="images/3.JPG" class="img-fluid img-thumbnail" style="max-width: 100%; height: auto; margin-top: 20px;">

						</div>

						<div class="col-md-6">

							<h2>無料体験レッスンのご要望はこちらへ:</h2>

							<hr>

							<p class="mail"><strong>メールアドレス:</strong></p><p class="mail-content">nils_skype@nilsph.com</p>

							<!--<p class="call"><strong>Call Us:</strong></p><p class="call-content">+81 3-4455-7714 (Mon to Fri 10:00AM-8:00PM JapanTime)</p>-->

							<!--<p class="incharge"><strong>In-Charge:</strong></p><p class="incharge-content">Hiroki Iwabuchi</p>-->

						</div>

						</div>

               		</div>

               </div>

     </section>

     

     <!-- TRIAL LESSON MODAL -->

     <?php include 'triallesson_modal.php';?>



     <!-- FOOTER -->

     <?php include 'footer.php'?>



     <!--AJAX -->

     <script type="text/javascript">

          $( "#formid" ).submit(function( event ) {

               event.preventDefault();



                $.ajax({

                  url: 'sendtriallesson',

                  type: 'POST',

                  data:  $('#formid').serialize(),

                  success: function(response) { 

                  if(response == 'Success') {  

                      $('#exampleModal').html("Success");

                      $('#exampleModal').modal('show'); //twitter bootstrap modal  

                  },

               });



               });

     </script>



     <!-- JAVASCIPTS && SCRIPTS -->

     <?php include 'link_scripts.php'?>



</body>

</html>