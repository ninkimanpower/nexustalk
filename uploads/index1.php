<?php include 'sendtriallesson.php'; ?>



<!DOCTYPE html>

<html lang="en">

<head>



     <title>NILS Online PH</title>

     <?php include 'header.php'?>

</head>

<body id="top" data-spy="scroll" data-target=".navbar-collapse" data-offset="50">



     <!-- PRE LOADER -->

     <section class="preloader">

          <div class="spinner">



               <span class="spinner-rotate"></span>

               

          </div>

     </section>





     <!-- MENU -->

     <section class="navbar custom-navbar navbar-fixed-top" role="navigation">

          <div class="container">



               <div class="navbar-header">

                    <button class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">

                         <span class="icon icon-bar"></span>

                         <span class="icon icon-bar"></span>

                         <span class="icon icon-bar"></span>

                    </button>



                    <!-- lOGO TEXT HERE -->

                    <!-- <a href="#" class="navbar-brand">Nils</a> -->

                    <a class="navbar-brand" href="#"><img class="navbar-logo" src="images/nilslogo3.png" width="130"></a>

               </div>



               <!-- MENU LINKS -->

               <!-- <div class="collapse navbar-collapse"> -->

               <div class="navbar-collapse">

                    <ul class="nav navbar-nav navbar-nav-first">

                         <li><a href="#top" class="smoothScroll">Home</a></li>

                         <li><a href="#about" class="smoothScroll">About NILS</a></li>

                         <!-- <li><a href="#courses" class="smoothScroll">Subjects</a>

                         </li> -->

                         <li class="nav-item dropdown show">

                       <!-- <a class="nav-link dropdown-toggle" href="#" role="button" id="navbarDropdown" data-toggle="dropdown" aria-haspopup="true" >

                         Subjects 

                       </a> -->

                       <a href="" class="nav-link dropdown-toggle" type="button" id="dropdownMenuLink"  aria-haspopup="true" aria-expanded="false" data-toggle="dropdown">

                            Subjects

                       </a>

                       <!-- <button class="nav-link dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true">

                            Subjects

                       </button> -->

                       

                       <div class="dropdown-menu" id="Dropdown" aria-labelledby="dropdownMenuLink" style="margin: auto;text-align: center;">

                              

                              <a class="dropdown-item" href="callan-method">Callan Method<br></a>

                              <a class="dropdown-item" href="speaking">Speaking<br></a>

                              <a class="dropdown-item" href="pronunciation">Pronunciation<br></a>

                             <!--  <a class="dropdown-item" href="#">Vocabulary</a> -->

                              <div class="dropdown-divider"></div>

                              <a class="dropdown-item" href="free-talking">Free Talking</a>

                              

                       </div>

                       

                     </li>

                         <li><a href="#team" class="smoothScroll">Our Teachers</a></li>

                         <!-- <li><a href="#courses" class="smoothScroll">Corporate / School Program</a></li> -->

                         <li><a href="schoolprograms" class="smoothScroll">Corporate / School Program</a></li>

                         <li><a href="#testimonial" class="smoothScroll">Reviews</a></li>

                    </ul>



                    <ul class="nav navbar-nav navbar-right">

                         <li><a href="#contact"><i class="fa fa-address-book-o"></i>Contact Us</a></li>

                         <li><a href="https://web.star7.jp/mypage/mobile_info.php?p=b0b8044427" target="_blank"><i class="fa fa-sign-in"></i>Log In</a></li>

                         <li><a href="../"><i class="fa fa-language"></i>Japanese</a></li>

                    </ul>

               </div>



          </div>

     </section>





     <!-- STICKY SOCIAL -->

     <?php include 'sticky_social.php';?>





     <!-- HOME -->

     <section id="home">

          <div class="row">



                    <div class="owl-carousel owl-theme home-slider">

                         <div class="item item-first">

                              <div class="caption">

                                   <div class="container">

                                        <div class="col-md-6 col-sm-12">

                                             <h1>Distance Education Learning Institution</h1>

                                             <h3>The top benefit of distance education is its flexibility. Students can choose when, where, and how they learn by selecting the time, place, and medium for their education.</h3>

                                             <a href="schoolprograms" class="section-btn btn btn-default smoothScroll">Discover more</a>

                                        </div>

                                   </div>

                              </div>

                         </div>



                         <div class="item item-second">

                              <div class="caption">

                                   <div class="container">

                                        <div class="col-md-6 col-sm-12">

                                             <h1>Start your journey with our Online Courses</h1>

                                             <h3>Our courses are well-created to help you improve and develop your English skills.</h3>

                                             <a data-toggle="modal" data-target="#exampleModal" class="section-btn btn btn-default smoothScroll">Take a course</a>

                                        </div>

                                   </div>

                              </div>

                         </div>



                         <div class="item item-third">

                              <div class="caption">

                                   <div class="container">

                                        <div class="col-md-6 col-sm-12">

                                             <h1>Efficient Learning Methods</h1>

                                             <h3>Our teachers has effective methods in teaching that help you assist and assess on your progress.</h3>

                                             <a href="#contact" class="section-btn btn btn-default smoothScroll">Let's chat</a>

                                        </div>

                                   </div>

                              </div>

                         </div>

                    </div>

          </div>

     </section>





            <section id="about">

          <div class="container">

               <div class="row mb-5">



                    <div class="col-md-6 col-sm-12">

                         <div class="about-info">

                              <h2>What is TESDA?</h2>

                              <hr>

                              <p class="text-tesda">TESDA stands for Technical Education and Skills Development Authority and is the government agency tasked to manage and supervise technical education and skills development (TESD) in the Philippines. </p><br>

                              <p class="text-tesda">One of their mandates is to develop an accreditation system for institutions involved in middle-level manpower development.</p>

                         <p class="text-tesda">Our school is accredited as an organization equivalent to the Ministry of Education, Culture, Sports, Science and Technology of Japan.

NILS is registered by TESDA since 2012. </p>

                         </div>

                    </div>



                    <div class="col-md-offset-1 col-md-4 col-sm-12">

                         <div class="entry-form">

                              <img src="images/tesda.png" width="260">

                         </div>

                    </div>



               </div>

               <div class="row">

                    <div class="col-md-3"></div>

                    <div class="col-md-6">

                         <div class="embed-responsive embed-responsive-16by9 mt-5">

                           <!-- <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/zpOULjyy-n8?rel=0" allowfullscreen></iframe> -->

                           <iframe width="1280" height="720" src="https://www.youtube.com/embed/pfx2MqV3I-w" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

                         </div>

                         <h3 class="text-center"><strong>Watch a sample of our Lesson</strong></h3>

                    </div>

                    <div class="col-md-3"></div>

               </div>

               <div class="row">

                    <div class="container">

                         <h2>What is NILS?</h2>

                         <hr>

                         <p class="text-nils">NILS stands for Newtype International Language School. It is an English language school which is established since 2011. It is located in IT Park, Salinas Drive Lahug, Cebu City.

NILS aims to provide good quality service towards giving English lessons to students. Its goal is to develop and enhance English skills of our students that will benefit them in the future.</p>

                                             <p class="text-nils">In 2018, we started providing online English conversation to students. Even now, it is still providing high quality classes to many individuals and corporations.</p>

                    </div>

               </div>

          </div>

          <div class="container">

               <div class="row">

                    <h2 class="text-center">Advantages of NILS</h2>

                    <div class="col-md-4 col-sm-4">

                         <div class="feature-thumb">

                              <span class="fa fa-clock-o" style="font-size: 60px;"></span>

                              <h3>Flexible Time of Reservation</h3>

                              <p>At any time, if  you like to  reserve and take  lessons, you may do! No hassle!</p>

                              <p>Reservations are also accepted 24 hours a day. Hurry now and book!</p>

                         </div>

                    </div>



                    <div class="col-md-4 col-sm-4">

                         <div class="feature-thumb">

                              <span class="fa fa-id-card-o" style="font-size: 40px;"></span>

                              <h3>Best Subjects for Distance Learning</h3>

                              <p>Choose subjects  according to your objectives.

 Hence, we can assist and assess your level on the trial lesson.</p>

                              <p>Our professional teachers and staff will help you determine your English level and recommend subjects to choose.</p>

                         </div>

                    </div>



                    <div class="col-md-4 col-sm-4">

                         <div class="feature-thumb" >

                              <span class="fa fa-certificate" style="font-size: 50px;"></span>

                              <h3>Continuous Learning Progress</h3>

                              <p>You can know  your progress from your weekly narrative report. </p>

                              <p>Students enrolled for a longer study period take the monthly test for the skills to be evaluated in terms of Grammar, Vocabulary, Reading and Speaking skills. Upon finishing the online course, a certificate of completion shall be given.</p>

                         </div>

                    </div>



               </div>

               <div class="page-content page-container text-center" id="page-content">

                   <div class="padding">

                       <div class="row container-bounce d-flex justify-content-center"> <button type="button" id="bouncebutton1" class="btn btn-success btn-icon-text bouncebutton1">Learn More</button> </div>

                   </div>

               </div>

          </div>

     </section>

     <hr>

     <!-- TEACHERS VIDEO -->

     <section>

          <div class="container">

               <div class="row">

                    <div class="col-md-12 col-sm-12">

                         <div class="section-title text-center">

                              <h2>Teacher <small>Video Lessons</small></h2>

                         </div>

                    </div>    

               </div>

               <div class="row">

                    <div class="container">

                         <div id="carousel-example-generic" class="carousel slide" data-interval="false">

                             

                             <div class="carousel-inner" role="listbox">

                               <div class="item active">

                                   <div class='carouselCont row no-gutter row-eq-height'>

                                   <div class="col-md-2"> 

                                   </div>

                                    <div class='col-md-8'>

                                      <div class="video-container" style="text-align: center;">

                                           <video class="video-bord" width="600" height="400" poster="images/nilslogo3.png" id="img-1">

                                                <source src="images/videos/video_1.mp4" type="video/mp4">

                                           </video>

                                           <div class="playpause"></div>

                                      </div>

                                   </div>

                                   <div class="col-md-2">

                                   </div>

                                   </div>

                                   <h2 class="text-center">Teacher Yeng</h2>

                                   <h4 class="text-center">Speaking Lesson</h4>

                               </div>



                               <div class="item">

                                   <div class='carouselCont row no-gutter row-eq-height'>

                                   <div class="col-md-2">     

                                   </div>

                                        <div class='col-md-8' >

                                           <div class="video-container" style="text-align: center;">

                                            <video class="video-bord" width="600" height="400" poster="images/nilslogo3.png" id="img-2">

                                                <source src="images/videos/video_2.mp4" type="video/mp4">

                                           </video>

                                           <div class="playpause"></div>

                                           </div>

                                        </div>

                                   <div class="col-md-2">     

                                   </div>

                                   </div>

                                   <h2 class="text-center">Teacher Yas</h2>

                                   <h4 class="text-center">Callan Lesson</h4>

                               </div>



                               <div class="item">

                                   <div class='carouselCont row no-gutter row-eq-height'>

                                   <div class="col-md-2">     

                                   </div>

                                        <div class='col-md-8' >

                                           <div class="video-container" style="text-align: center;">

                                            <video class="video-bord" width="600" height="400" poster="images/nilslogo3.png" id="img-3">

                                                <source src="images/videos/video_7.mp4" type="video/mp4">

                                           </video>

                                           <div class="playpause"></div>

                                           </div>

                                        </div>

                                   <div class="col-md-2">     

                                   </div>

                                   </div>

                                   <h2 class="text-center">Teacher Claud</h2>

                                   <h4 class="text-center">Speaking Lesson</h4>

                               </div>



                               <div class="item">

                                   <div class='carouselCont row no-gutter row-eq-height'>

                                   <div class="col-md-2">     

                                   </div>

                                        <div class='col-md-8' >

                                           <div class="video-container" style="text-align: center;">

                                            <video class="video-bord" width="600" height="400" poster="images/nilslogo3.png" id="img-4">

                                                <source src="images/videos/video_4.mp4" type="video/mp4">

                                           </video>

                                           <div class="playpause"></div>

                                           </div>

                                        </div>

                                   <div class="col-md-2">     

                                   </div>

                                   </div>

                                   <h2 class="text-center">Teacher Karen</h2>

                                   <h4 class="text-center">Pronunciation Lesson</h4>

                               </div>



                               <div class="item">

                                   <div class='carouselCont row no-gutter row-eq-height'>

                                   <div class="col-md-2">     

                                   </div>

                                        <div class='col-md-8' >

                                           <div class="video-container" style="text-align: center;">

                                            <video class="video-bord" width="600" height="400" poster="images/nilslogo3.png" id="img-5">

                                                <source src="images/videos/video_5.mp4" type="video/mp4">

                                           </video>

                                           <div class="playpause"></div>

                                           </div>

                                        </div>

                                   <div class="col-md-2">     

                                   </div>

                                   </div>

                                   <h2 class="text-center">Teacher Joy</h2>

                                   <h4 class="text-center">Pronunciation Lesson</h4>

                               </div>



                               <div class="item">

                                   <div class='carouselCont row no-gutter row-eq-height'>

                                   <div class="col-md-2">     

                                   </div>

                                        <div class='col-md-8' >

                                           <div class="video-container" style="text-align: center;">

                                            <video class="video-bord" width="600" height="400" poster="images/nilslogo3.png" id="img-6">

                                                <source src="images/videos/video_6.mp4" type="video/mp4">

                                           </video>

                                           <div class="playpause"></div>

                                           </div>

                                        </div>

                                   <div class="col-md-2">     

                                   </div>

                                   </div>

                                   <h2 class="text-center">Teacher Lits</h2>

                                   <h4 class="text-center">Speaking Lesson</h4>

                               </div>

                               

                             </div>



                             <!-- Controls -->

                              <div class='ctrls'>

                                  <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">

                                    <i class="fa fa-caret-left" aria-hidden="true" title="Previous"></i>

                                  </a>

                                  <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">

                                    <i class="fa fa-caret-right" aria-hidden="true" title="Next"></i>

                                  </a>

                             </div>

                             

                           </div>



                           <div style="margin-right: 41px;">

                                <ul>

                                    <li class="nav-dots">

                                         <label for="img-1" class="nav-dot" id="img-dot-1"></label>

                                         <label for="img-2" class="nav-dot" id="img-dot-2"></label>

                                         <label for="img-3" class="nav-dot" id="img-dot-3"></label>

                                         <label for="img-4" class="nav-dot" id="img-dot-4"></label>

                                         <label for="img-5" class="nav-dot" id="img-dot-5"></label>

                                         <label for="img-6" class="nav-dot" id="img-dot-6"></label>

                                    </li>  

                                </ul>

                         </div>

                    </div>

               </div>









          </div>

     </section>



     <!-- TEAM -->

    <section id="team">

          <div class="container">

               <div class="row">



                    <div class="col-md-12 col-sm-12">

                         <div class="section-title">

                              <h2>Teachers <small>Meet Professional Trainers</small></h2>

                         </div>

                    </div>



                    <div class="col-md-3 col-sm-6">

                         <div class="team-thumb">

                              <div class="team-image">

                                   <img src="../../../images/teacher-1.JPEG" class="img-responsive" alt="">

                              </div>

                              <div class="team-info">

                                   <h3>Karen Mae Deniega</h3>

                                   <span>"Hello everyone! Don't stop learning English. Study with us. I assure you that you'll have fun in learning and with perseverance to study, you'll definitely achieve your goal!"</span>

                              </div>

                              <ul class="social-icon">

                                   <li><a href="#" class="fa fa-facebook-square" attr="facebook icon"></a></li>

                                   <li><a href="#" class="fa fa-twitter"></a></li>

                                   <li><a href="#" class="fa fa-instagram"></a></li>

                              </ul>

                         </div>

                    </div>



                    <div class="col-md-3 col-sm-6">

                         <div class="team-thumb">

                              <div class="team-image">

                                   <img src="../../../images/teacher-2.JPEG" class="img-responsive" alt="">

                              </div>

                              <div class="team-info">

                                   <h3>Clarisse Ann Lopez</h3>

                                   <span>"Young or old, it's never too late to learn the language. Book your lessons today! That's one of the best decisions you'll ever make this year."</span>

                              </div>

                              <ul class="social-icon">

                                   <li><a href="#" class="fa fa-google"></a></li>

                                   <li><a href="#" class="fa fa-instagram"></a></li>

                              </ul>

                         </div>

                    </div>



                    <div class="col-md-3 col-sm-6">

                         <div class="team-thumb">

                              <div class="team-image">

                                   <img src="../../../images/teacher-3.JPEG" class="img-responsive" alt="">

                              </div>

                              <div class="team-info">

                                   <h3>Ariel Cabillo</h3>

                                   <span>"Let's learn English and have fun! I'll be waiting for you!"</span>

                              </div>

                              <ul class="social-icon">

                                   <li><a href="#" class="fa fa-twitter"></a></li>

                                   <li><a href="#" class="fa fa-envelope-o"></a></li>

                                   <li><a href="#" class="fa fa-linkedin"></a></li>

                              </ul>

                         </div>

                    </div>



                    <div class="col-md-3 col-sm-6">

                         <div class="team-thumb">

                              <div class="team-image">

                                   <img src="../../../images/teacher-4.JPEG" class="img-responsive" alt="">

                              </div>

                              <div class="team-info">

                                   <h3>Vanjoe Cabaluna</h3>

                                   <span>"Study like there's no tomorrow and always push yourself that you can do more. Be the best you can be."</span>

                              </div>

                              <ul class="social-icon">

                                   <li><a href="#" class="fa fa-twitter"></a></li>

                                   <li><a href="#" class="fa fa-google"></a></li>

                                   <li><a href="#" class="fa fa-behance"></a></li>

                              </ul>

                         </div>

                    </div>



               </div>

               <div class="row">

                    <div class="page-content page-container text-center" id="page-content">

                        <div class="padding">

                            <div class="row container-bounce d-flex justify-content-center"> <button type="button" id="bouncebutton3" class="btn btn-success btn-icon-text bouncebutton3">View Teachers</button> </div>

                        </div>

                    </div>

               </div>

          </div>

     </section>



<section>

  <div class="row">

    <div class="container">

      <div class="col-md-12 col-sm-12">

         <div class="section-title">

              <h2 class="text-center">Get To Know More About Us</h2>

         </div>

    </div>

  </div>

  </div>

  <div class="container">

    <div class="html5gallery" data-skin="gallery" data-width="480" data-height="272" >

      

      <a href="https://youtu.be/lUHO0XOy434"><img src="http://i3.ytimg.com/vi/lUHO0XOy434/maxresdefault.jpg" alt="Enroll Now!"></a>

      <a href="https://youtu.be/meScKu9vdLE"><img src="http://i3.ytimg.com/vi/meScKu9vdLE/maxresdefault.jpg" alt="What we offer?"></a>

      <a href="https://youtu.be/gIRJaQEm81E"><img src="http://i3.ytimg.com/vi/gIRJaQEm81E/maxresdefault.jpg" alt="Why learn English language?"></a>

      <a href="https://youtu.be/qVOtgVR_Ipw"><img src="http://i3.ytimg.com/vi/qVOtgVR_Ipw/maxresdefault.jpg" alt="Learn from the best School and teachers"></a>

      <a href="https://youtu.be/E0FlH1hzRy4"><img src="http://i3.ytimg.com/vi/E0FlH1hzRy4/maxresdefault.jpg" alt="Why we are the best school?"></a>

      <a href="https://youtu.be/xZswx04kVDQ"><img src="http://i3.ytimg.com/vi/xZswx04kVDQ/maxresdefault.jpg" alt="Skype Teaching"></a>

      <a href="https://youtu.be/qArxPIyKkxk"><img src="http://i3.ytimg.com/vi/qArxPIyKkxk/maxresdefault.jpg" alt="What is Callan?"></a>

      <a href="https://youtu.be/FFusCC501X8"><img src="http://i3.ytimg.com/vi/FFusCC501X8/maxresdefault.jpg" alt="Low score Exams? No problem."></a>

      <a href="https://youtu.be/rB3bvwWa_8g"><img src="http://i3.ytimg.com/vi/rB3bvwWa_8g/maxresdefault.jpg" alt="Quality Internship program?"></a>

      

    </div>

  </div>

</section>









     <!-- TESTIMONIAL -->

     <section id="testimonial">

          <div class="container">

               <div class="row">



                    <div class="col-md-12 col-sm-12">

                         <div class="section-title">

                              <h2>Student Reviews<small><hr>from around the world</small></h2>

                         </div>



                         <div class="owl-carousel owl-theme owl-client">

                            

                      <?php 

                          require_once 'dbconnection.php';



                          $query = $databaseconnection->query("SELECT * FROM review_en WHERE islocked = 0");



                          $details = [];



                          while ($row = $query->fetch_object()) {

                            $details[] = $row;

                          }





                          foreach ($details as $reviews) {



                          $starNumber = $reviews->star_number;

                      ?>

                                <div class="col-md-4 col-sm-4">

                                   <div class="item">

                                        <div class="tst-image">

                                             <img src="images/reviews-image.jpg" class="img-responsive" alt="">

                                        </div>

                                        <div class="tst-author">

                                             <h4 style="font-size: 16px;">Initial： <?php echo $reviews->initial;?></h4>

                                             <span>Age: <?php echo $reviews->age;?></span>

                                        </div>

                                        <p><?php echo $reviews->review_comment;?></p>

                                        

                                        <div class="tst-rating">



                                        <?php

                                          for($i=1;$i<=$starNumber;$i++){

                                            echo '<i class="fa fa-star"></i>';

                                          }if (strpos($starNumber,'.')) {

                                            echo '<i class="fa fa-star-half-o"></i>';

                                            $i++;

                                          }

                                          while ($i<=5) {

                                             echo '<i class="fa fa-star-o"></i>';

                                             $i++;

                                          }

                                        ?>

                                        </div>

                                   </div>

                                </div>

                      <?php

                        }

                      ?>





                         </div>



               </div>

          </div>

     </div>

     </section> 





     <!-- CONTACT -->

     <section id="contact">

          <div class="container">

               <div class="row">



                    <div class="col-md-6 col-sm-12">

                         <form id="contact-form" role="form" action="" method="post">

                              <div class="section-title">

                                   <h2>Contact us <small>we love conversations. let us talk!</small></h2>

                              </div>



                              <div class="col-md-12 col-sm-12">

                                   <input type="text" class="form-control" placeholder="Enter full name" name="name" required="">

                    

                                   <input type="email" class="form-control" placeholder="Enter email address" name="email" required="">



                                   <textarea class="form-control" rows="6" placeholder="Tell us about your message" name="message" required=""></textarea>

                              </div>



                              <div class="col-md-4 col-sm-12">

                                   <input type="submit" class="form-control" name="send message" value="Send Message">

                              </div>



                         </form>

                    </div>



                    <div class="col-md-6 col-sm-12">

                         <div class="contact-image">

                              <img src="../../../images/carousel/2.JPG" class="img-responsive" alt="Smiling Two Girls">

                         </div>

                    </div>



               </div>

          </div>

     </section>





     <!-- TRIAL LESSON MODAL -->

     <?php include 'triallesson_modal.php';?>







     <!-- FOOTER -->

     <?php include 'footer.php'?>



     <!-- TESTING SCRIPT 1-->

<!--      <script type="text/javascript">

          $(".video-thumb").click(function() {

          $('.video-thumb > img').removeClass("active");

          $(this).children('img').addClass("active");

        });



        $('div.video-thumb').click(function() {

          $('.video-iframe iframe').attr('src', ($(this).children('iframe').attr('src').replace('iframe')));

        });

     </script> -->



     <!-- PLAYPAUSE -->

     <script type="text/javascript">

          $('.video-bord').parent().click(function () {

            if($(this).children(".video-bord").get(0).paused){        

               $(this).children(".video-bord").get(0).play();   

               $(this).children(".playpause").fadeOut();

              }else{       $(this).children(".video-bord").get(0).pause();

            $(this).children(".playpause").fadeIn();

              }

          });

     </script>







     <!--AJAX -->

     <script type="text/javascript">

          $( "#formid" ).submit(function( event ) {

               event.preventDefault();



                $.ajax({

                  url: 'sendtriallesson',

                  type: 'POST',

                  data:  $('#formid').serialize(),

                  success: function(response) { 

                  if(response == 'Success') {  

                      $('#exampleModal').html("Success");

                      $('#exampleModal').modal('show'); //twitter bootstrap modal  

                  }

               });



               });

     </script>



     <!-- JAVASCRIPT && SCRIPTS -->

     <?php include 'link_scripts.php'?>

</body>

</html>