<?php include 'sendtriallesson.php'; ?>



<!DOCTYPE html>

<html lang="en">

<head>



     <title>NILS Online PH</title>

     <?php include 'header.php'?>

</head>

<body id="top" data-spy="scroll" data-target=".navbar-collapse" data-offset="50">



     <!-- PRE LOADER -->

     <section class="preloader">

          <div class="spinner">



               <span class="spinner-rotate"></span>

               

          </div>

     </section>





     <!-- MENU -->

     <section class="navbar custom-navbar navbar-fixed-top" role="navigation">

          <div class="container">



               <div class="navbar-header">

                    <button class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">

                         <span class="icon icon-bar"></span>

                         <span class="icon icon-bar"></span>

                         <span class="icon icon-bar"></span>

                    </button>



                    <!-- lOGO TEXT HERE -->

                    <!-- <a href="#" class="navbar-brand">Nils</a> -->

                    <a class="navbar-brand" href="#"><img class="navbar-logo" src="images/nilslogo3.png" width="130"></a>

               </div>



               <!-- MENU LINKS -->

               <!-- <div class="collapse navbar-collapse"> -->

               <div class="navbar-collapse">

                    <ul class="nav navbar-nav navbar-nav-first">

                         <li><a href="#top" class="smoothScroll">ホーム</a></li>

                         <li><a href="#about" class="smoothScroll">NILSについて</a></li>

                         <!-- <li><a href="#courses" class="smoothScroll">Subjects</a>

                         </li> -->

                         <li class="nav-item dropdown show">

                       <!-- <a class="nav-link dropdown-toggle" href="#" role="button" id="navbarDropdown" data-toggle="dropdown" aria-haspopup="true" >

                         Subjects 

                       </a> -->

                       <a href="" class="nav-link dropdown-toggle" type="button" id="dropdownMenuLink"  aria-haspopup="true" aria-expanded="false" data-toggle="dropdown">

                            科目

                       </a>

                       <!-- <button class="nav-link dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true">

                            Subjects

                       </button> -->

                       

                       <div class="dropdown-menu" id="Dropdown" aria-labelledby="dropdownMenuLink" style="margin: auto;text-align: center;">

                              

                              <a class="dropdown-item" href="callan-method">カランメソッド<br></a>

                              <a class="dropdown-item" href="speaking">スピーキング<br></a>

                              <a class="dropdown-item" href="pronunciation">発音<br></a>

                             <!--  <a class="dropdown-item" href="#">Vocabulary</a> -->

                              <div class="dropdown-divider"></div>

                              <a class="dropdown-item" href="free-talking">フリートーキング/スピーチ</a>

                              

                       </div>

                       

                     </li>

                         <li><a href="#team" class="smoothScroll">講師紹介</a></li>

                         <!-- <li><a href="#courses" class="smoothScroll">Corporate / School Program</a></li> -->

                         <li><a href="schoolprograms" class="smoothScroll">法人様・教育機関担当者様へ</a></li>

                         <li><a href="#testimonial" class="smoothScroll">生徒様の声</a></li>

                    </ul>



                    <ul class="nav navbar-nav navbar-right">

                         <li><a href="#contact"><i class="fa fa-address-book-o"></i>お問い合わせ</a></li>

                         <li><a href="https://web.star7.jp/mypage/mobile_info.php?p=b0b8044427" target="_blank"><i class="fa fa-sign-in"></i>Log In</a></li>

                         <li><a href="en"><i class="fa fa-language"></i>English</a></li>

                    </ul>

               </div>



          </div>

     </section>





     <!-- STICKY SOCIAL -->

     <?php include 'sticky_social.php';?>





     <!-- HOME -->

     <section id="home">

          <div class="row">



                    <div class="owl-carousel owl-theme home-slider">

                         <div class="item item-first">

                              <div class="caption">

                                   <div class="container">

                                        <div class="col-md-6 col-sm-12">

                                             <h1>わたしたちはオンライン英会話レッスンを提供する語学学校です。</h1>

                                             <h3>当校のオンライン英会話の最大の利点は、柔軟性です。 生徒は、場所、媒体、いつ、どこで、どのように学習するかを選択できます。</h3>

                                             <a href="schoolprograms" class="section-btn btn btn-default smoothScroll">もっと見る</a>

                                        </div>

                                   </div>

                              </div>

                         </div>



                         <div class="item item-second">

                              <div class="caption">

                                   <div class="container">

                                        <div class="col-md-6 col-sm-12">

                                             <h1>オンライン英会話で旅を始めよう！</h1>

                                             <h3>私たちのコースは、英語力の向上を必須条件として作成されています。</h3>

                                             <a data-toggle="modal" data-target="#exampleModal" class="section-btn btn btn-default smoothScroll">今すぐ受講する</a>

                                        </div>

                                   </div>

                              </div>

                         </div>



                         <div class="item item-third">

                              <div class="caption">

                                   <div class="container">

                                        <div class="col-md-6 col-sm-12">

                                             <h1>効率的な学習方法</h1>

                                             <h3>在籍講師は、あなたの進捗を常に確認し、評価をします。生徒の進捗によって、効果的な教授法を習得しています。</h3>

                                             <a href="#contact" class="section-btn btn btn-default smoothScroll">Let's chat</a>

                                        </div>

                                   </div>

                              </div>

                         </div>

                    </div>

          </div>

     </section>





          <section id="about">

          <div class="container">

               <div class="row mb-5">



                    <div class="col-md-6 col-sm-12">

                         <div class="about-info">

                              <h2>TESDAってなに？</h2>

                              <hr>

                              <p class="text-tesda">TESDAは、Technical Education and Skills Development Authorityの略で、フィリピンの技術教育およびスキル開発（TESD）の管理と監督を任務とする政府機関です。</p><br>

                              <p class="text-tesda">彼らの使命の1つは、中堅レベルの人材育成に関与する機関の認定システムを開発することです。</p>

                         <p class="text-tesda">当校は、現地のプレイベートスクールとして、2012年に文部科学省に相当する機関として認定されています。</p>

                         </div>

                    </div>



                    <div class="col-md-offset-1 col-md-4 col-sm-12">

                         <div class="entry-form">

                              <img src="images/tesda.png" width="260">

                         </div>

                    </div>



               </div>

               <div class="row">

                    <div class="col-md-3"></div>

                    <div class="col-md-6">

                         <div class="embed-responsive embed-responsive-16by9 mt-5">

                           <!-- <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/zpOULjyy-n8?rel=0" allowfullscreen></iframe> -->

                           <iframe width="1280" height="720" src="https://www.youtube.com/embed/pfx2MqV3I-w" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

                         </div>

                         <h3 class="text-center"><strong>Watch a sample of our Lesson</strong></h3>

                    </div>

                    <div class="col-md-3"></div>

               </div>

               <div class="row">

                    <div class="container">

                         <h2>NILSってなに？</h2>

                         <hr>

                         <p class="text-nils">NILSはNewtypeInternational LanguageSchoolの略です。 2011年にセブ市のITパークに設立された英語学校です。 NILSは、学生に英語のレッスンを提供するための質の高いサービスを提供しています。 将来世界中で活躍するための人材育成を目指しており、それを強化することが最大のポイントです。</p>

                                             <p class="text-nils">2018年には、学生へのオンライン英会話の提供を開始しました。 現在でも、多くの個人や企業に質の高いクラスを提供しています。</p>

                    </div>

               </div>

          </div>

          <div class="container">

               <div class="row">

                    <h2 class="text-center">NILSであるべき理由</h2>

                    <div class="col-md-4 col-sm-4">

                         <div class="feature-thumb">

                              <span class="fa fa-clock-o" style="font-size: 60px;"></span>

                              <h3>柔軟な予約時間</h3>

                              <p>レッスンを受けたい場合は、いつでも予約が出来ます。</p>

                              <p>24時間予約が可能です。急いで予約をしよう！</p>

                         </div>

                    </div>



                    <div class="col-md-4 col-sm-4">

                         <div class="feature-thumb">

                              <span class="fa fa-id-card-o" style="font-size: 40px;"></span>

                              <h3>オンライン英会話に最適な科目</h3>

                              <p>目的に応じて科目を選択してください。 </p>

                              <p>レベルチェックであなたに最適な科目を提案します。</p>

                         </div>

                    </div>



                    <div class="col-md-4 col-sm-4">

                         <div class="feature-thumb" >

                              <span class="fa fa-certificate" style="font-size: 50px;"></span>

                              <h3>継続的な進捗報告</h3>

                              <p>毎週成績表と講評を送ることで、自身の進捗状況を知ることが出来ます。</p>

                              <p>長期の生徒はマンスリーテストの結果を踏まえた、より詳細な進捗が確認できます。

                              卒業時には、卒業証明書が発行されます。

                              </p>

                         </div>

                    </div>



               </div>

               <div class="page-content page-container text-center" id="page-content">

                   <div class="padding">

                       <div class="row container-bounce d-flex justify-content-center"> <button type="button" id="bouncebutton1" class="btn btn-success btn-icon-text bouncebutton1">Learn More</button> </div>

                   </div>

               </div>

          </div>

     </section>

     <hr>

     <!-- TEACHERS VIDEO -->

     <section>

          <div class="container">

               <div class="row">

                    <div class="col-md-12 col-sm-12">

                         <div class="section-title text-center">

                              <h2>Teacher <small>Video Lessons</small></h2>

                         </div>

                    </div>    

               </div>

               <div class="row">

                    <div class="container">

                         <div id="carousel-example-generic" class="carousel slide" data-interval="false">

                             

                             <div class="carousel-inner" role="listbox">

                               <div class="item active">

                                   <div class='carouselCont row no-gutter row-eq-height'>

                                   <div class="col-md-2"> 

                                   </div>

                                    <div class='col-md-8'>

                                      <div class="video-container" style="text-align: center;">

                                           <video class="video-bord" width="600" height="400" poster="images/nilslogo3.png" id="img-1">

                                                <source src="images/videos/video_1.mp4" type="video/mp4">

                                           </video>

                                           <div class="playpause"></div>

                                      </div>

                                   </div>

                                   <div class="col-md-2">

                                   </div>

                                   </div>

                                   <h2 class="text-center">Teacher Yeng</h2>

                                   <h4 class="text-center">Speaking Lesson</h4>

                               </div>



                               <div class="item">

                                   <div class='carouselCont row no-gutter row-eq-height'>

                                   <div class="col-md-2">     

                                   </div>

                                        <div class='col-md-8' >

                                           <div class="video-container" style="text-align: center;">

                                            <video class="video-bord" width="600" height="400" poster="images/nilslogo3.png" id="img-2">

                                                <source src="images/videos/video_2.mp4" type="video/mp4">

                                           </video>

                                           <div class="playpause"></div>

                                           </div>

                                        </div>

                                   <div class="col-md-2">     

                                   </div>

                                   </div>

                                   <h2 class="text-center">Teacher Yas</h2>

                                   <h4 class="text-center">Callan Lesson</h4>

                               </div>



                               <div class="item">

                                   <div class='carouselCont row no-gutter row-eq-height'>

                                   <div class="col-md-2">     

                                   </div>

                                        <div class='col-md-8' >

                                           <div class="video-container" style="text-align: center;">

                                            <video class="video-bord" width="600" height="400" poster="images/nilslogo3.png" id="img-3">

                                                <source src="images/videos/video_7.mp4" type="video/mp4">

                                           </video>

                                           <div class="playpause"></div>

                                           </div>

                                        </div>

                                   <div class="col-md-2">     

                                   </div>

                                   </div>

                                   <h2 class="text-center">Teacher Claud</h2>

                                   <h4 class="text-center">Speaking Lesson</h4>

                               </div>



                               <div class="item">

                                   <div class='carouselCont row no-gutter row-eq-height'>

                                   <div class="col-md-2">     

                                   </div>

                                        <div class='col-md-8' >

                                           <div class="video-container" style="text-align: center;">

                                            <video class="video-bord" width="600" height="400" poster="images/nilslogo3.png" id="img-4">

                                                <source src="images/videos/video_4.mp4" type="video/mp4">

                                           </video>

                                           <div class="playpause"></div>

                                           </div>

                                        </div>

                                   <div class="col-md-2">     

                                   </div>

                                   </div>

                                   <h2 class="text-center">Teacher Karen</h2>

                                   <h4 class="text-center">Pronunciation Lesson</h4>

                               </div>



                               <div class="item">

                                   <div class='carouselCont row no-gutter row-eq-height'>

                                   <div class="col-md-2">     

                                   </div>

                                        <div class='col-md-8' >

                                           <div class="video-container" style="text-align: center;">

                                            <video class="video-bord" width="600" height="400" poster="images/nilslogo3.png" id="img-5">

                                                <source src="images/videos/video_5.mp4" type="video/mp4">

                                           </video>

                                           <div class="playpause"></div>

                                           </div>

                                        </div>

                                   <div class="col-md-2">     

                                   </div>

                                   </div>

                                   <h2 class="text-center">Teacher Joy</h2>

                                   <h4 class="text-center">Pronunciation Lesson</h4>

                               </div>



                               <div class="item">

                                   <div class='carouselCont row no-gutter row-eq-height'>

                                   <div class="col-md-2">     

                                   </div>

                                        <div class='col-md-8' >

                                           <div class="video-container" style="text-align: center;">

                                            <video class="video-bord" width="600" height="400" poster="images/nilslogo3.png" id="img-6">

                                                <source src="images/videos/video_6.mp4" type="video/mp4">

                                           </video>

                                           <div class="playpause"></div>

                                           </div>

                                        </div>

                                   <div class="col-md-2">     

                                   </div>

                                   </div>

                                   <h2 class="text-center">Teacher Lits</h2>

                                   <h4 class="text-center">Speaking Lesson</h4>

                               </div>

                               

                             </div>



                             <!-- Controls -->

                              <div class='ctrls'>

                                  <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">

                                    <i class="fa fa-caret-left" aria-hidden="true" title="Previous"></i>

                                  </a>

                                  <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">

                                    <i class="fa fa-caret-right" aria-hidden="true" title="Next"></i>

                                  </a>

                             </div>

                             

                           </div>



                           <div style="margin-right: 41px;">

                                <ul>

                                    <li class="nav-dots">

                                         <label for="img-1" class="nav-dot" id="img-dot-1"></label>

                                         <label for="img-2" class="nav-dot" id="img-dot-2"></label>

                                         <label for="img-3" class="nav-dot" id="img-dot-3"></label>

                                         <label for="img-4" class="nav-dot" id="img-dot-4"></label>

                                         <label for="img-5" class="nav-dot" id="img-dot-5"></label>

                                         <label for="img-6" class="nav-dot" id="img-dot-6"></label>

                                    </li>  

                                </ul>

                         </div>

                    </div>

               </div>









          </div>

     </section>



     <!-- TEAM -->

     <section id="team">

          <div class="container">

               <div class="row">



                    <div class="col-md-12 col-sm-12">

                         <div class="section-title">

                              <h2>Teachers <small>Meet Professional Trainers</small></h2>

                         </div>

                    </div>



                    <div class="col-md-3 col-sm-6">

                         <div class="team-thumb">

                              <div class="team-image">

                                   <img src="images/teacher-1.JPEG" class="img-responsive" alt="">

                              </div>

                              <div class="team-info">

                                   <h3>Karen Mae Deniega</h3>

                                   <span>みなさん、こんにちは。英語を学ぶのをやめないでください。私たちと一緒に勉強してください。楽しく学び、忍耐強く勉強することで、間違いなく目標を達成できます。</span>

                              </div>

                         </div>

                    </div>



                    <div class="col-md-3 col-sm-6">

                         <div class="team-thumb">

                              <div class="team-image">

                                   <img src="images/teacher-2.JPEG" class="img-responsive" alt="">

                              </div>

                              <div class="team-info">

                                   <h3>Clarisse Ann Lopez</h3>

                                   <span>老若男女問わず、言語を学ぶのに遅いも早いもありません。今すぐやりましょう！きっとそれは今年の中の最高の決断になるでしょう。</span>

                              </div>

                         </div>

                    </div>



                    <div class="col-md-3 col-sm-6">

                         <div class="team-thumb">

                              <div class="team-image">

                                   <img src="images/teacher-3.JPEG" class="img-responsive" alt="">

                              </div>

                              <div class="team-info">

                                   <h3>Ariel Cabillo</h3>

                                   <span>英語を学んで楽しみましょう！お待ちしております！</span>

                              </div>

                         </div>

                    </div>



                    <div class="col-md-3 col-sm-6">

                         <div class="team-thumb">

                              <div class="team-image">

                                   <img src="images/teacher-4.JPEG" class="img-responsive" alt="">

                              </div>

                              <div class="team-info">

                                   <h3>Vanjoe Cabaluna</h3>

                                   <span>今日1日を最も大事にして勉強して、自分を追い込んでください。できる限りのことをすることであなたの夢が得られるでしょう。</span>

                              </div>

                         </div>

                    </div>



               </div>

               <div class="row">

                    <div class="page-content page-container text-center" id="page-content">

                        <div class="padding">

                            <div class="row container-bounce d-flex justify-content-center"> <button type="button" id="bouncebutton3" class="btn btn-success btn-icon-text bouncebutton3">View Teachers</button> </div>

                        </div>

                    </div>

               </div>

          </div>

     </section>



<section>

  <div class="row">

    <div class="container">

      <div class="col-md-12 col-sm-12">

         <div class="section-title">

              <h2 class="text-center">Get To Know More About Us</h2>

         </div>

    </div>

  </div>

  </div>

  <div class="container">

    <div class="html5gallery" data-skin="gallery" data-width="480" data-height="272" >

      

      <a href="https://youtu.be/lUHO0XOy434"><img src="http://i3.ytimg.com/vi/lUHO0XOy434/maxresdefault.jpg" alt="Enroll Now!"></a>

      <a href="https://youtu.be/meScKu9vdLE"><img src="http://i3.ytimg.com/vi/meScKu9vdLE/maxresdefault.jpg" alt="What we offer?"></a>

      <a href="https://youtu.be/gIRJaQEm81E"><img src="http://i3.ytimg.com/vi/gIRJaQEm81E/maxresdefault.jpg" alt="Why learn English language?"></a>

      <a href="https://youtu.be/qVOtgVR_Ipw"><img src="http://i3.ytimg.com/vi/qVOtgVR_Ipw/maxresdefault.jpg" alt="Learn from the best School and teachers"></a>

      <a href="https://youtu.be/E0FlH1hzRy4"><img src="http://i3.ytimg.com/vi/E0FlH1hzRy4/maxresdefault.jpg" alt="Why we are the best school?"></a>

      <a href="https://youtu.be/xZswx04kVDQ"><img src="http://i3.ytimg.com/vi/xZswx04kVDQ/maxresdefault.jpg" alt="Skype Teaching"></a>

      <a href="https://youtu.be/qArxPIyKkxk"><img src="http://i3.ytimg.com/vi/qArxPIyKkxk/maxresdefault.jpg" alt="What is Callan?"></a>

      <a href="https://youtu.be/FFusCC501X8"><img src="http://i3.ytimg.com/vi/FFusCC501X8/maxresdefault.jpg" alt="Low score Exams? No problem."></a>

      <a href="https://youtu.be/rB3bvwWa_8g"><img src="http://i3.ytimg.com/vi/rB3bvwWa_8g/maxresdefault.jpg" alt="Quality Internship program?"></a>

      

    </div>

  </div>

</section>









     <!-- TESTIMONIAL -->

     <section id="testimonial">

          <div class="container">

               <div class="row">



                    <div class="col-md-12 col-sm-12">

                         <div class="section-title">

                              <h2>生徒からの声<small><hr></small></h2>

                         </div>



                         <div class="owl-carousel owl-theme owl-client">

                            

                      <?php 

                          require_once 'dbconnection.php';

                            

                          $query = $databaseconnection->query("SELECT * FROM reviews WHERE islocked = 0");



                          $details = [];



                          while ($row = $query->fetch_object()) {

                            $details[] = $row;

                          }





                          foreach ($details as $reviews) {



                          $starNumber = $reviews->star_number;

                      ?>

                                <div class="col-md-4 col-sm-4">

                                   <div class="item">

                                        <div class="tst-image">

                                             <img src="images/reviews-image.jpg" class="img-responsive" alt="">

                                        </div>

                                        <div class="tst-author">

                                             <h4 style="font-size: 16px;">イニシャル： <?php echo $reviews->initial;?></h4>

                                             <span>年齢: <?php echo $reviews->age;?></span>

                                        </div>

                                        <p><?php echo $reviews->review_comment;?></p>

                                        

                                        <div class="tst-rating">



                                        <?php

                                          for($i=1;$i<=$starNumber;$i++){

                                            echo '<i class="fa fa-star"></i>';

                                          }if (strpos($starNumber,'.')) {

                                            echo '<i class="fa fa-star-half-o"></i>';

                                            $i++;

                                          }

                                          while ($i<=5) {

                                             echo '<i class="fa fa-star-o"></i>';

                                             $i++;

                                          }

                                        ?>

                                        </div>

                                   </div>

                                </div>

                      <?php

                        }

                      ?>





                         </div>



               </div>

          </div>

     </div>

     </section> 





     <!-- CONTACT -->

     <section id="contact">

          <div class="container">

               <div class="row">



                    <div class="col-md-6 col-sm-12">

                         <form id="contact-form" role="form" action="" method="post">

                              <div class="section-title">

                                   <h2>Contact us <small>we love conversations. let us talk!</small></h2>

                              </div>



                              <div class="col-md-12 col-sm-12">

                                   <input type="text" class="form-control" placeholder="Enter full name" name="name" required="">

                    

                                   <input type="email" class="form-control" placeholder="Enter email address" name="email" required="">



                                   <textarea class="form-control" rows="6" placeholder="Tell us about your message" name="message" required=""></textarea>

                              </div>



                              <div class="col-md-4 col-sm-12">

                                   <input type="submit" class="form-control" name="send message" value="Send Message">

                              </div>



                         </form>

                    </div>



                    <div class="col-md-6 col-sm-12">

                         <div class="contact-image">

                              <img src="images/carousel/2.JPG" class="img-responsive" alt="Smiling Two Girls">

                         </div>

                    </div>



               </div>

          </div>

     </section>





     <!-- TRIAL LESSON MODAL -->

     <?php include 'triallesson_modal.php';?>







     <!-- FOOTER -->

     <?php include 'footer.php'?>



     <!-- TESTING SCRIPT 1-->

<!--      <script type="text/javascript">

          $(".video-thumb").click(function() {

          $('.video-thumb > img').removeClass("active");

          $(this).children('img').addClass("active");

        });



        $('div.video-thumb').click(function() {

          $('.video-iframe iframe').attr('src', ($(this).children('iframe').attr('src').replace('iframe')));

        });

     </script> -->



     <!-- PLAYPAUSE -->

     <script type="text/javascript">

          $('.video-bord').parent().click(function () {

            if($(this).children(".video-bord").get(0).paused){        

               $(this).children(".video-bord").get(0).play();   

               $(this).children(".playpause").fadeOut();

              }else{       $(this).children(".video-bord").get(0).pause();

            $(this).children(".playpause").fadeIn();

              }

          });

     </script>







     <!--AJAX -->

     <script type="text/javascript">

          $( "#formid" ).submit(function( event ) {

               event.preventDefault();



                $.ajax({

                  url: 'sendtriallesson',

                  type: 'POST',

                  data:  $('#formid').serialize(),

                  success: function(response) { 

                  if(response == 'Success') {  

                      $('#exampleModal').html("Success");

                      $('#exampleModal').modal('show'); //twitter bootstrap modal  

                  }

               });



               });

     </script>



     <!-- JAVASCRIPT && SCRIPTS -->

     <?php include 'link_scripts.php'?>

</body>

</html>