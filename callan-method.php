<?php include 'sendtriallesson.php';?>

<!DOCTYPE html>
<html lang="en">
<head>
	<title>NILS Online - Callan Method</title>
	<?php include 'header.php'?>
</head>
<body data-spy="scroll" data-target=".navbar-collapse" data-offset="50">
     <!-- PRE LOADER -->
     <section class="preloader">
          <div class="spinner">

               <span class="spinner-rotate"></span>
               
          </div>
     </section>
     <!-- Navbar -->
     <?php include 'navbar.php';?>

     <!-- STICKY SOCIAL -->
     <?php include 'sticky_social.php';?>

     <!-- PARALLAX EFFECT -->
      <div class="parallax-callan-upper"></div>

     <!-- SECTION START -->
<!--      <section> -->
      <div style="background-color: #e9ecef;">
     	<div class="container">
     		<div class="row">
     			<div class="col-md-12 col-sm-12">
                         <div class="section-title text-center">
                              <h2 class="callan-title" style="margin-top: 5px;">カランメソッド<hr style="border: 1px solid black"><small class="smol">英語のスキルのバランスをとる。</small></h2>
                         </div>
                </div>   
     		</div>
     		<div class="row">
     			<p class="callan-text">カランメソッドは、1960年代に開発された、英語を教えるための直接的なアプローチです。 それは英語を勉強するための速くて効果的な方法です。 この科目の目的は、生徒が考えずに英語を話すようにすることです。 生徒は最初のレッスンから英語で話します。 この方法は高度に構造化された指導プログラムであり、生徒はアクティブで楽しくダイナミックな方法で英語で話し、読み、理解し、書くことをすばやく学ぶことができます。 カランメソッドを使用している学校は、ヨーロッパ、アジア、ラテンアメリカを含む世界中にあります。 これは、世界中の100万人をはるかに超える人々が、英語で効果的かつ自信を持って英語で効果的にコミュニケーションするのに役立ちました。</p>
     		</div>
     	</div>
     </div>
      <div class="parallax-callan"></div>
      <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <div class="section-title text-center">
                    <h3 class="callan-subtitle">このレッスンの目的</h3>
                    <hr>
                </div>
            </div>
        </div>
        <div class="row">
            <p class="callan-text-second">NILSのESLプログラムの主な目的は、英語を母国語としない人が英語を話す高等教育環境でうまく機能できるように準備することです。 そのため、このプログラムの目的は次のとおりです。</p>
        </div>
        <div class="row">
          <div class="col-md">
             <p class="callan-text-second"><i class="fa fa-check-circle-o fa-3x" aria-hidden="true"></i>リスニング、スピーキング、リーディング、ライティングのスキルを向上させます。</p>
          </div>
          <div class="col-md">
             <p class="callan-text-second"><i class="fa fa-check-circle-o fa-3x" aria-hidden="true"></i>考えずに反射的に英語で話す能力を養います。</p>
          </div>
          <div class="col-md">
             <p class="callan-text-second"><i class="fa fa-check-circle-o fa-3x" aria-hidden="true"></i>英語を流暢に話せるようになります。</p>
          </div>
          <div class="col-md">
             <p class="callan-text-second"><i class="fa fa-check-circle-o fa-3x" aria-hidden="true"></i>効果的なコミュニケーションのために話す際に、文法的に正しく、状況に基づいた、文化的に適切な言語を話せるようになります。</p>
          </div>
        </div>
      </div>
      <div style="background-color: #e9ecef;">
      <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <div class="section-title text-center">
                    <h3 class="callan-subtitle">コースの条件</h3>
                    <hr style="border: 1px solid black">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md">
              <p class="callan-text-second"><i class="fa fa-check-square-o fa-3x" aria-hidden="true"></i>英語の基礎知識と各種テストでのスコアアップに必要なスキルを身に着けられる。</p>
            </div>
            <div class="col-md">
              <p class="callan-text-second"><i class="fa fa-check-square-o fa-3x" aria-hidden="true"></i>欠席をしないこと</p>
            </div>
            <div class="col-md">
              <p class="callan-text-second"><i class="fa fa-check-square-o fa-3x" aria-hidden="true"></i>7歳以上</p>
            </div>
            <div class="col-md">
              <p class="callan-text-second"><i class="fa fa-check-square-o fa-3x" aria-hidden="true"></i>カランメソッド専用の教科書をステージごとに購入する</p>
            </div>
        </div>
      </div>
     	</div>
      <div class="container">
        <div class="row">
          <div class="col-md-12 col-sm-12">
            <div class="section-title text-center">
              <h3 class="callan-subtitle">コースの詳細</h3>
              <hr>            
            </div>
          </div>
        </div>
        <div class="row">
          <p class="callan-text-second" style="margin-bottom: 50px;">このクラスは、生徒の理解を深め、英語を流ちょう話すことを目的としています。 主な焦点は、話すこと、聞くこと、そして発音です。 生徒はステージ1から12までの迅速な質問と回答で英語に没頭し、理解を深め、母国語を考えて翻訳する時間がなくなります。 しばらくすると、彼らは英語で直接考え、話すようになります！ 講師による彼らの間違いの絶え間ない体系的な修正は、話すときの彼らの発音と自信を大いに改善します。 また、レッスンでは読み書きができるので、4つのスキルすべてを練習します。 筆記試験と口頭試験は各段階の完了後に行われるため、進行状況を簡単に追跡できます。 カランメソッドは、スピード、修正、繰り返しに焦点を当てています。</p>
        </div>
      </div>
<!--      </section> -->


     <!-- TRIAL LESSON MODAL -->
     <?php include 'triallesson_modal.php';?>

     <!-- FOOTER -->
     <?php include 'footer.php'?>

     <!--AJAX -->
     <script type="text/javascript">
          $( "#formid" ).submit(function( event ) {
               event.preventDefault();

                $.ajax({
                  url: 'sendtriallesson',
                  type: 'POST',
                  data:  $('#formid').serialize(),
                  success: function(response) { 
                  if(response == 'Success') {  
                      $('#exampleModal').html("Success");
                      $('#exampleModal').modal('show'); //twitter bootstrap modal  
                  },
               });

               });
     </script>

     <!-- JAVASCIPTS && SCRIPTS -->
     <?php include 'link_scripts.php'?>

</body>
</html>