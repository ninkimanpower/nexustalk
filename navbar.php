<?php  

  $urlpage = $_SERVER['REQUEST_URI'];

?>

<!-- MENU -->

    <section class="navbar custom-navbar navbar-fixed-top" role="navigation">

          <div class="container">



               <div class="navbar-header">

                    <button class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">

                         <span class="icon icon-bar"></span>

                         <span class="icon icon-bar"></span>

                         <span class="icon icon-bar"></span>

                    </button>



                    <!-- lOGO TEXT HERE -->

                    <!-- <a href="#" class="navbar-brand">Nils</a> -->

                    <a class="navbar-brand" href="index"><img class="navbar-logo" src="images/NexusTALK_logo.png"></a>

               </div>



               <!-- MENU LINKS -->

              <!--  <div class="collapse navbar-collapse"> -->

               <div class="navbar-collapse">

                    <ul class="nav navbar-nav navbar-nav-first">

                         <li><a href="index#top" class="smoothScroll">ホーム</a></li>

                         <li><a href="index#about" class="smoothScroll">NILSについて</a></li>

                         <!-- <li><a href="#courses" class="smoothScroll">Subjects</a>

                         </li> -->

                         <li class="nav-item dropdown show">

                       <!-- <a class="nav-link dropdown-toggle" href="#" role="button" id="navbarDropdown" data-toggle="dropdown" aria-haspopup="true" >

                         Subjects

                       </a> -->

                       <a href="" class="nav-link dropdown-toggle" type="button" id="dropdownMenuButton"  aria-haspopup="true" data-toggle="dropdown">

                            科目

                       </a>

                       <div class="dropdown-menu" id="Dropdown" aria-labelledby="navbarDropdown" style="margin: auto;text-align: center;">

                         

                              <a class="dropdown-item" href="callan-method">カランメソッド<br></a>

                              <a class="dropdown-item" href="speaking">スピーキング<br></a>

                              <a class="dropdown-item" href="pronunciation">発音<br></a>

                             <!--  <a class="dropdown-item" href="#">Vocabulary</a> -->

                              <div class="dropdown-divider"></div>

                              <a class="dropdown-item" href="free-talking">フリートーキング/スピーチ</a>

                         
                              
                       </div>

                       

                     </li>

                         <li><a href="index#team" class="smoothScroll">講師紹介</a></li>

                         <!-- <li><a href="index#courses" class="smoothScroll">Corporate / School Program</a></li> -->

                         <li><a href="schoolprograms" class="smoothScroll">法人様・教育機関担当者様へ</a></li>

                         <li><a href="index#testimonial" class="smoothScroll">生徒様の声</a></li>

                    </ul>



                    <ul class="nav navbar-nav navbar-right">

                         <li><a href="#contact"><i class="fa fa-address-book-o"></i>お問い合わせ</a></li>

                         <li><a href="https://web.star7.jp/mypage/mobile_info.php?p=b0b8044427" target="_blank"><i class="fa fa-sign-in"></i>Log In</a></li>

                         <li>

                        <?php  

                            if ($urlpage == '/nils-website/aboutnils') {

                              ?>

                                  <a href="en/aboutnils"><i class="fa fa-language"></i>English</a>

                              <?php

                            }else if ($urlpage == '/callan-method') {

                              ?>

                                  <a href="en/callan-method"><i class="fa fa-language"></i>English</a>

                              <?php

                            }else if ($urlpage == '/free-talking') {

                              ?>

                                  <a href="en/free-talking"><i class="fa fa-language"></i>English</a>

                              <?php

                            }else if ($urlpage == '/pronunciation') {

                              ?>

                                  <a href="en/pronunciation"><i class="fa fa-language"></i>English</a>

                              <?php

                            }else if ($urlpage == '/nils-website/schoolprograms') {

                              ?>

                                  <a href="en/schoolprograms"><i class="fa fa-language"></i>English</a>

                              <?php

                            }else if ($urlpage == '/speaking') {

                              ?>

                                  <a href="en/speaking"><i class="fa fa-language"></i>English</a>

                              <?php

                            }else if ($urlpage == '/teachers') {

                              ?>

                                  <a href="en/teachers"><i class="fa fa-language"></i>English</a>

                              <?php

                            }

                         ?>

                         </li>

                    </ul>

               </div>



          </div>

     </section>