<?php include 'sendtriallesson.php';?>

<!DOCTYPE html>
<html lang="en">
<head>
	<title>NILS Online - Callan Method</title>
	<?php include 'header.php'?>
</head>
<body data-spy="scroll" data-target=".navbar-collapse" data-offset="50">
     <!-- PRE LOADER -->
     <section class="preloader">
          <div class="spinner">

               <span class="spinner-rotate"></span>
               
          </div>
     </section>
     <!-- Navbar -->
     <?php include 'navbar.php';?>

     <!-- STICKY SOCIAL -->
     <?php include 'sticky_social.php';?>

     <!-- PARALLAX EFFECT -->
      <div class="parallax-callan-upper"></div>

     <!-- SECTION START -->
<!--      <section> -->
       <div style="background-color: #e9ecef;">
      <div class="container">
        <div class="row">
          <div class="col-md-12 col-sm-12">
                         <div class="section-title text-center">
                              <h2 class="callan-title" style="margin-top: 5px;">Callan Method<hr style="border: 1px solid black"><small class="smol">To have balance in your skill on the English Language</small></h2>
                         </div>
                </div>   
        </div>
        <div class="row">
          <p class="callan-text">The Callan Method is a direct approach to teach English, developed in the 1960s. It is a fast and effective method for studying English. The purpose of the Method is to engage students in speaking English without thinking. Students speak in English from the very first lesson. The method is a highly structured program of instruction, which allows students to quickly learn to speak, read, comprehend and write in English in an active, fun, and dynamic way. Schools using the Callan Method can be found all over the world including Europe, Asian, and Latin America. It has helped well over a million people around the world to communicate in English effectively in English effectively and confidently.</p>
        </div>
      </div>
     </div>
      <div class="parallax-callan"></div>
      <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <div class="section-title text-center">
                    <h3 class="callan-subtitle">Learning Objectives</h3>
                    <hr>
                </div>
            </div>
        </div>
        <div class="row">
            <p class="callan-text-second">The primary objective of NILS' ESL program is to prepare non-native speakers of English to successfully function in an English speaking post-secondary environment. As such, the program aims to:</p>
        </div>
        <div class="row">
          <div class="col-md">
             <p class="callan-text-second"><i class="fa fa-check-circle-o fa-3x" aria-hidden="true"></i>Improve their Listening, Speaking, Reading and Writing skills.</p>
          </div>
          <div class="col-md">
             <p class="callan-text-second"><i class="fa fa-check-circle-o fa-3x" aria-hidden="true"></i>Develop the ability to speak in English reflexively without thinking.</p>
          </div>
          <div class="col-md">
             <p class="callan-text-second"><i class="fa fa-check-circle-o fa-3x" aria-hidden="true"></i>Develop their English fluency.</p>
          </div>
          <div class="col-md">
             <p class="callan-text-second"><i class="fa fa-check-circle-o fa-3x" aria-hidden="true"></i>Expand the learner's use of grammatically correct, situation-based and culturally appropriate language in speaking for effective communication.</p>
          </div>
        </div>
      </div>
      <div style="background-color: #e9ecef;">
      <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <div class="section-title text-center">
                    <h3 class="callan-subtitle">Course Requirements</h3>
                    <hr style="border: 1px solid black">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md">
              <p class="callan-text-second"><i class="fa fa-check-square-o fa-3x" aria-hidden="true"></i>Basic knowledge in the English language and the skills needed to perform well during the evaluation test</p>
            </div>
            <div class="col-md">
              <p class="callan-text-second"><i class="fa fa-check-square-o fa-3x" aria-hidden="true"></i>Zero absences</p>
            </div>
            <div class="col-md">
              <p class="callan-text-second"><i class="fa fa-check-square-o fa-3x" aria-hidden="true"></i>Age 7 and up</p>
            </div>
            <div class="col-md">
              <p class="callan-text-second"><i class="fa fa-check-square-o fa-3x" aria-hidden="true"></i>Callan stage course book</p>
            </div>
        </div>
      </div>
      </div>
      <div class="container">
        <div class="row">
          <div class="col-md-12 col-sm-12">
            <div class="section-title text-center">
              <h3 class="callan-subtitle">Course Details</h3>
              <hr>            
            </div>
          </div>
        </div>
        <div class="row">
          <p class="callan-text-second" style="margin-bottom: 50px;">This class is designed to improve the students understanding, and spoken English. The main focus is on speaking, listening, and pronunciation. The students will be immersed in English with rapid questions and answers from stage 1-12 which improve their understanding and allow them no time to think and translate into their native language. After a short time, they will begin to think and speak directly in English! Constant systematic correction of their mistakes, by the teacher, greatly improves their pronunciation and confidence when speaking. They will also have some reading and writing in the lessons, so they will practice all four skills. Written and oral exams follow the completion of each stage, so it is easy to follow their progress. Callan Method focuses on SPEED, CORRECTION and REPITITION.</p>
        </div>
      </div>
<!--      </section> -->


     <!-- TRIAL LESSON MODAL -->
     <?php include 'triallesson_modal.php';?>

     <!-- FOOTER -->
     <?php include 'footer.php'?>

     <!--AJAX -->
     <script type="text/javascript">
          $( "#formid" ).submit(function( event ) {
               event.preventDefault();

                $.ajax({
                  url: 'sendtriallesson',
                  type: 'POST',
                  data:  $('#formid').serialize(),
                  success: function(response) { 
                  if(response == 'Success') {  
                      $('#exampleModal').html("Success");
                      $('#exampleModal').modal('show'); //twitter bootstrap modal  
                  },
               });

               });
     </script>

     <!-- JAVASCIPTS && SCRIPTS -->
     <?php include 'link_scripts.php'?>

</body>
</html>