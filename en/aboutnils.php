<?php include 'sendtriallesson.php'; ?>



<!DOCTYPE html>

<html lang="en">

<head>

	<title>NILS Online - About Nils</title>

  <?php include 'header.php'?>

</head>

<body data-spy="scroll" data-target=".navbar-collapse" data-offset="50">

     <!-- PRE LOADER -->

     <section class="preloader">

          <div class="spinner">



               <span class="spinner-rotate"></span>

               

          </div>

     </section>



     <!-- Navbar -->

     <?php include 'navbar.php';?>



     <!-- STICKY SOCIAL -->

     <?php include 'sticky_social.php';?>

     

     <section id="about">

     	       <div class="container">

               <div class="row mb-5">



                    <div class="col-md-6 col-sm-12">

                         <div class="about-info">

                              <h2>What is TESDA?</h2>

                              <hr>

                              <p class="text-tesda">TESDA stands for Technical Education and Skills Development Authority, a government agency responsible for managing and overseeing technical education and skills development (TESD) in the Philippines.</p><br>

                              <p class="text-tesda">One of their missions is to develop a certification system for institutions involved in mid-level talent development.</p>

                         <p class="text-tesda">NILS under the umbrella of JICC is accredited as an organization equivalent to the Ministry of Education, Culture, Sports, Science and Technology. NILS has been registered with TESDA since 2012.</p>

                         </div>

                    </div>



                    <div class="col-md-offset-1 col-md-4 col-sm-12">

                         <div class="entry-form">

                              <img src="images/tesda.png" width="260">

                         </div>

                    </div>



               </div>

               <div class="row">

                    <div class="col-md-3"></div>

                    <div class="col-md-6">

                         <div class="embed-responsive embed-responsive-16by9 mt-5">

                           <!-- <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/zpOULjyy-n8?rel=0" allowfullscreen></iframe> -->

                           <iframe width="1280" height="720" src="https://www.youtube.com/embed/pfx2MqV3I-w" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

                         </div>

                         <h3 class="text-center"><strong>Watch a sample of our Lesson</strong></h3>

                    </div>

                    <div class="col-md-3"></div>

               </div>

               <div class="row">

                    <div class="container">

                         <h2>What is NILS?</h2>

                         <hr>

                         <p class="text-nils">NILS, a subsidiary of JICC, stands for Newtype International Language School. It is an English school established in 2011. It is located in the IT Park of Salinas Drive La Hug, Cebu City. NILS aims to provide quality services for providing English lessons to students. The goal is to develop and enhance the English skills of our students, which will benefit them in the future.</p>

                                             <p class="text-nils">In 2018, we started offering online English conversation to students. Utilizing that know-how from April 2021, JICC has taken over the online English conversation and still provides high quality classes to many individuals and companies.</p>

                    </div>

               </div>

               <div class="row">

               		<div class="container">

               			<h2 class="text-center">Experience A High Quality Lessons</h2>

               			<p class="about-text">Try it for FREE!<br>

							If you want to experience good quality lessons with our skilled teachers, reserve for a trial lesson.<br>

							We invite you to unlimited free trial lessons. (You can choose one-on-one class.)</p>

						<div class="row">

						<div class="col-md-6">

							<img src="images/3.JPG" class="img-fluid img-thumbnail" style="max-width: 100%; height: auto; margin-top: 20px;">

						</div>

						<div class="col-md-6">

							<h2>To Request for a Trial Lesson, Contact us Below:</h2>

							<hr>

							<p class="mail"><strong>Send us an E-mail:</strong></p><p class="mail-content">nils_skype@nilsph.com</p>

							<p class="call"><strong>Call Us:</strong></p><p class="call-content">+81 3-4455-7714 (Mon to Fri 10:00AM-8:00PM JapanTime)</p>

							<p class="incharge"><strong>In-Charge:</strong></p><p class="incharge-content">Hiroki Iwabuchi</p>

						</div>

						</div>

               		</div>

               </div>

     </section>

     

     <!-- TRIAL LESSON MODAL -->

     <?php include 'triallesson_modal.php';?>



     <!-- FOOTER -->

     <?php include 'footer.php'?>



     <!--AJAX -->

     <script type="text/javascript">

          $( "#formid" ).submit(function( event ) {

               event.preventDefault();



                $.ajax({

                  url: 'sendtriallesson',

                  type: 'POST',

                  data:  $('#formid').serialize(),

                  success: function(response) { 

                  if(response == 'Success') {  

                      $('#exampleModal').html("Success");

                      $('#exampleModal').modal('show'); //twitter bootstrap modal  

                  },

               });



               });

     </script>



     <!-- JAVASCIPTS && SCRIPTS -->

     <?php include 'link_scripts.php'?>



</body>

</html>