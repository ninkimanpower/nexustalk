<?php include 'sendtriallesson.php';?>

<!DOCTYPE html>
<html lang="en">
<head>
	<title>NILS Online - Free Talking / Speech </title>
	<?php include 'header.php'?>
</head>
<body data-spy="scroll" data-target=".navbar-collapse" data-offset="50">
     <!-- PRE LOADER -->
     <section class="preloader">
          <div class="spinner">

               <span class="spinner-rotate"></span>
               
          </div>
     </section>
     <!-- Navbar -->
     <?php include 'navbar.php';?>

     <!-- STICKY SOCIAL -->
     <?php include 'sticky_social.php';?>

     <!-- PARALLAX EFFECT -->
      <div class="parallax-freetalking-upper"></div>

     <!-- SECTION START -->
<!--      <section> -->

     	      <div class="container">
        <div class="row">
          <div class="col-md-12 col-sm-12">
                         <div class="section-title text-center">
                              <h2 class="callan-title" style="margin-top: 5px;">Free Talking / Speech<hr style="border: 1px solid black"><small class="smol">It is recommended for communications during business trips, travels and tours.</small></h2>
                         </div>
                </div>   
        </div>
        <div class="row">
          <p class="callan-text">You can learn phrases and words during the classes. The class aims to improve your ability to respond to questions. It also encourages you to express your feelings and answers in English.</p>
        </div>
      </div>
      <div style="background-color: #e9ecef;">
      <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <div class="section-title text-center">
                    <h3 class="callan-subtitle">Course Requirements</h3>
                    <hr style="border: 1px solid black">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md">
              <p class="callan-text-second"><i class="fa fa-check-square-o fa-3x" aria-hidden="true"></i>Basic knowledge in the English language and the skills needed to perform well during the evaluation test</p>
            </div>
            <div class="col-md">
              <p class="callan-text-second"><i class="fa fa-check-square-o fa-3x" aria-hidden="true"></i>Kids and Adults</p>
            </div>
        </div>
      </div>
      </div>
<!--      </section> -->


    <!-- TRIAL LESSON MODAL -->
     <?php include 'triallesson_modal.php';?>

     <!-- FOOTER -->
     <?php include 'footer.php'?>

     <!--AJAX -->
     <script type="text/javascript">
          $( "#formid" ).submit(function( event ) {
               event.preventDefault();

                $.ajax({
                  url: 'sendtriallesson',
                  type: 'POST',
                  data:  $('#formid').serialize(),
                  success: function(response) { 
                  if(response == 'Success') {  
                      $('#exampleModal').html("Success");
                      $('#exampleModal').modal('show'); //twitter bootstrap modal  
                  },
               });

               });
     </script>

     <!-- JAVASCIPTS && SCRIPTS -->
     <?php include 'link_scripts.php'?>

</body>
</html>