
     <!-- Modal -->
     <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
       <div class="modal-dialog" role="document">
         <div class="modal-content">
           <div class="modal-header">
             <h3 class="modal-title" id="exampleModalLabel">Fill up the form for a Trial Lesson :</h3>
           </div>
           <div class="modal-body">
               <form method="POST" id="formid" enctype="multipart/form-data">
                 <div class="form-group">
                      <label for="name">Full Name :</label>
                      <input type="text" name="name" class="form-control" id="name" aria-describedby="nameHelp" placeholder="Name" required>
                 </div>
                 <div class="form-group">
                   <label for="email">Email address :</label>
                   <input type="email" name="email" class="form-control" id="email" aria-describedby="emailHelp" placeholder="Enter email" required>
                   <!-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
                 </div>
                 <div class="form-group">
                    <div class="row">
                         <div class="col-sm-6">
                             <label for="number">TEL :</label>
                             <div class='input-group'>
                              <input type="number" name="number" class="form-control" id="number" placeholder="TEL Number" required>
                              <span class="input-group-addon">
                                   </span>
                             </div>
                         </div>
                         <div class="col-sm-6">
                              <label for="datetime-local">Desired Date & Time :</label>
                              <div class='input-group'>
                                   <input type='datetime-local' name="date" min="<?php echo $finDate; echo $minTime;?>" class="form-control" required/>
                                   <span class="input-group-addon">
                                   </span>
                              </div>
                         </div>
                   </div>
                 </div>
                 <div class="modal-footer">
                  <div class="modal-text">
                  <span class="modal-note">Please Note*</span><p>Reservation must be scheduled a week before the requested trial class.</p><p>Schedule: Monday - Friday only (Weekdays)</p>
                  <p>Time: 10 A.M - 10 P.M only (Japan Time)</p>
                  </div>
                    <?php echo $alert; ?>
                      <button type="submit" name="submit" class="btn btn-success btn-sm" style="font-size: 20px;">Submit</button>
                 </div>
               </form>
           </div>
         </div>
       </div>
     </div>  