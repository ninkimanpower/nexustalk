<?php include 'sendtriallesson.php';?>

<!DOCTYPE html>
<html lang="en">
<head>
	<title>NILS Online - Pronunciation</title>
	<?php include 'header.php'?>
</head>
<body data-spy="scroll" data-target=".navbar-collapse" data-offset="50">
     <!-- PRE LOADER -->
     <section class="preloader">
          <div class="spinner">

               <span class="spinner-rotate"></span>
               
          </div>
     </section>
     <!-- Navbar -->
     <?php include 'navbar.php';?>

     <!-- STICKY SOCIAL -->
     <?php include 'sticky_social.php';?>

     <!-- PARALLAX EFFECT -->
      <div class="parallax-pronunciation-upper"></div>

     <!-- SECTION START -->
<!--      <section> -->

     	      <div class="container">
        <div class="row">
          <div class="col-md-12 col-sm-12">
                         <div class="section-title text-center">
                              <h2 class="callan-title" style="margin-top: 5px;">Pronunciation<hr style="border: 1px solid black"><small class="smol">Highly recommended to learn the proper pronunciation of words.</small></h2>
                         </div>
                </div>   
        </div>
        <div class="row">
          <p class="callan-text">The focus of this class is for students to learn the proper positioning of the tongue in pronunciation. Teachers demonstrate a basic understanding of vowel and consonant sound production with a focus on improving individual problem areas related to first language. Understand fundamental patterns of stress, rhythm, and intonation. 
A selection of materials and techniques drawn from phonetics enhances and stimulates a raised awareness of various patterns and meanings.</p>
        </div>
      </div>

      <div style="background-color: #e9ecef;">
      <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <div class="section-title text-center">
                    <h3 class="callan-subtitle">Course Requirements</h3>
                    <hr style="border: 1px solid black">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md">
              <p class="callan-text-second"><i class="fa fa-check-square-o fa-3x" aria-hidden="true"></i>Basic knowledge in the English language and the skills needed to perform well during the evaluation test</p>
            </div>
            <div class="col-md">
              <p class="callan-text-second"><i class="fa fa-check-square-o fa-3x" aria-hidden="true"></i>Kids and Adults</p>
            </div>
        </div>
      </div>
      </div>

<!--      </section> -->


     <!-- TRIAL LESSON MODAL -->
     <?php include 'triallesson_modal.php';?>

     <!-- FOOTER -->
     <?php include 'footer.php'?>

     <!--AJAX -->
     <script type="text/javascript">
          $( "#formid" ).submit(function( event ) {
               event.preventDefault();

                $.ajax({
                  url: 'sendtriallesson',
                  type: 'POST',
                  data:  $('#formid').serialize(),
                  success: function(response) { 
                  if(response == 'Success') {  
                      $('#exampleModal').html("Success");
                      $('#exampleModal').modal('show'); //twitter bootstrap modal  
                  },
               });

               });
     </script>

     <!-- JAVASCIPTS && SCRIPTS -->
     <?php include 'link_scripts.php'?>

</body>
</html>