<?php  

  $urlpage = $_SERVER['REQUEST_URI'];

?>

<!-- MENU -->

    <section class="navbar custom-navbar navbar-fixed-top" role="navigation">

          <div class="container">



               <div class="navbar-header">

                    <button class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">

                         <span class="icon icon-bar"></span>

                         <span class="icon icon-bar"></span>

                         <span class="icon icon-bar"></span>

                    </button>



                    <!-- lOGO TEXT HERE -->

                    <!-- <a href="#" class="navbar-brand">Nils</a> -->

                    <a class="navbar-brand" href="index"><img class="navbar-logo" src="images/NexusTALK_logo.png" width="150"></a>

               </div>



               <!-- MENU LINKS -->

              <!--  <div class="collapse navbar-collapse"> -->

               <div class="navbar-collapse">

                    <ul class="nav navbar-nav navbar-nav-first">

                         <li><a href="index#top" class="smoothScroll">Home</a></li>

                         <li><a href="index#about" class="smoothScroll">About NILS</a></li>
                         
                         <!-- <li><a href="#courses" class="smoothScroll">Subjects</a>

                         </li> -->

                         <li class="nav-item dropdown show">

                       <!-- <a class="nav-link dropdown-toggle" href="#" role="button" id="navbarDropdown" data-toggle="dropdown" aria-haspopup="true" >

                         Subjects

                       </a> -->

                       <a href="" class="nav-link dropdown-toggle" type="button" id="dropdownMenuButton"  aria-haspopup="true" data-toggle="dropdown">

                            Subjects

                       </a>

                       <div class="dropdown-menu" id="Dropdown" aria-labelledby="navbarDropdown" style="margin: auto;text-align: center;">

                         

                              <a class="dropdown-item" href="callan-method">Callan Method<br></a>

                              <a class="dropdown-item" href="speaking">Speaking<br></a>

                              <a class="dropdown-item" href="pronunciation">Pronunciation<br></a>

                             <!--  <a class="dropdown-item" href="#">Vocabulary</a> -->

                              <div class="dropdown-divider"></div>

                              <a class="dropdown-item" href="free-talking">Free Talking</a>

                         

                       </div>

                       

                     </li>

                         <li><a href="index#team" class="smoothScroll">Our Teachers</a></li>

                         <!-- <li><a href="index#courses" class="smoothScroll">Corporate / School Program</a></li> -->

                         <li><a href="schoolprograms" class="smoothScroll">Corporate / School Program</a></li>

                         <li><a href="index#testimonial" class="smoothScroll">Reviews</a></li>

                    </ul>



                    <ul class="nav navbar-nav navbar-right">

                         <li><a href="#contact"><i class="fa fa-address-book-o"></i>Contact Us</a></li>

                         <li><a href="https://web.star7.jp/mypage/mobile_info.php?p=b0b8044427" target="_blank"><i class="fa fa-sign-in"></i>Log In</a></li>

                         <li>

                             <?php  

                                if ($urlpage == '/nils-website/en/aboutnils') {

                                  ?>

                                      <a href="../aboutnils"><i class="fa fa-language"></i>Japanese</a>

                                  <?php

                                }else if ($urlpage == '/nils-website/en/callan-method') {

                                  ?>

                                      <a href="../callan-method"><i class="fa fa-language"></i>Japanese</a>

                                  <?php

                                }else if ($urlpage == '/en/free-talking') {

                                  ?>

                                      <a href="../free-talking"><i class="fa fa-language"></i>Japanese</a>

                                  <?php

                                }else if ($urlpage == '/en/pronunciation') {

                                  ?>

                                      <a href="../pronunciation"><i class="fa fa-language"></i>Japanese</a>

                                  <?php

                                }else if ($urlpage == '/nils-online/en/schoolprograms') {

                                  ?>

                                      <a href="../schoolprograms"><i class="fa fa-language"></i>Japanese</a>

                                  <?php

                                }else if ($urlpage == '/en/speaking') {

                                  ?>

                                      <a href="../speaking"><i class="fa fa-language"></i>Japanese</a>

                                  <?php

                                }else if ($urlpage == '/en/teachers') {

                                  ?>

                                      <a href="../teachers"><i class="fa fa-language"></i>Japanese</a>

                                  <?php

                                }

                             ?>

                         </li>

                    </ul>

               </div>



          </div>

     </section>