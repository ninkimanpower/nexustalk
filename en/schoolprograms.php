<?php include 'sendtriallesson.php'; ?>

<!DOCTYPE html>
<html>
<head>
     <title>NILS Online - Corporate/School Programs</title>
     <?php include 'header.php'?>
</head>
<body data-spy="scroll" data-target=".navbar-collapse" data-offset="50">

     <!-- PRE LOADER -->
     <section class="preloader">
          <div class="spinner">

               <span class="spinner-rotate"></span>
               
          </div>
     </section>


     <?php include 'navbar.php';?>

     <!-- STICKY SOCIAL -->
     <?php include 'sticky_social.php';?>
     
     <section id="courses" class="courses" >
          <div class="container">
               <h2>NILS Online starts at a reasonable price.</h2>
               <hr>
               <div class="row">
                    <div class="col-md-6">
                         <img src="images/3.JPG" class="img-fluid img-thumbnail" style="max-width: 100%; height: auto;">
                    </div>
                    <div class="col-md-6 bg-color">
                        <h3>1 month for $20</h3>
                        <p><i class="fa fa-check">Plan 4 times a month. Good for 4 classes having 25 minutes each class.</i></p>
                        <p><i class="fa fa-check">We will provide maximum flexibility such as creating a special plan according to the  school and corporation.</i></p>
                        <div class="border-0">
                             <p>Just download the world-class free application, "Skype". Communication app used in the Philippines  ⇄ </p>
                              <a href="https://www.skype.com/en/get-skype/" target="_blank"><img src="images/skype.png" class="img-thumbnail" style="max-width: 15%"></a><span>&#8592; *Click to Download Skype*</span><br>
                              <span>* Free registration of the reservation site is required. A management number will be assigned to each company.</span>
                        </div>
                        <h4><u>NILS Online is easy to get started.</u></h4>
                    </div>
               </div>
               <h2><u>A</u>&nbsp<i class="fa fa-caret-square-o-down" style="color: #29ca8e;"></i></h2>
               <div class="row">
                    <!-- TABLE 1 -->
                    <table class="table table-striped table-bordered bg-color">
                      <thead>
                        <tr>
                          <th scope="col"></th>
                          <th scope="col">Regular Lessons and Extracurricular Lessons</th>
                          <th scope="col">Study Abroad at Home</th>
                          <th scope="col">Study Abroad</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <th scope="row">Purpose</th>
                          <td>Group lessons as a new teaching tool for English.</td>
                          <td>Perform a fixed program using long vacations and accept credits.</td>
                          <td>Perform a fixed program using long vacations and accept credits.</td>
                        </tr>
                        <tr>
                          <th scope="row">Cost</th>
                          <td>Group lessons as a new teaching tool for English. </td>
                          <td>Partial school -Payment, individual payment</td>
                          <td>Personal Payment (Special discount)</td>
                        </tr>
                        <tr>
                          <th scope="row">Course</th>
                          <td> ・ Use up 50 frames for 5 months <br>・ Twice a week group lesson<br>・ Credits for 10 lessons.</td>
                          <td>・ Fixed lessons of 4-6 hours a day for 2 weeks. Graduation certificate and weekly narrative progress  report  will be issued for  regular study abroad.</td>
                          <td>・ Possible from 1 week <br>・ Select 4-10 times a day <br>・ You can take online lessons for free before you travel.</td>
                        </tr>
                      </tbody>
                    </table>
               </div>
               <hr>
               <div class="row">
                    <div class="col-sm-4">
                         <!-- <i class="fa fa-graduation-cap fa-10x" aria-hidden="true"></i> -->
                         <img src="images/school-icon.png" class="img-thumbnail" style="max-width: 90%;">
                    </div>
                    <div class="col-sm-4 bg-color">
                         <h3><u>Partnered Schools Who Have Taken Classes:</u></h3>   
                         <ul style="font-size: 15px;">
                              <li>Tokuyama National College of Technology</li>
                              <li>Osaka Institute of Technology International Exchange Center</li>
                              <li>Kanda Foreign Language Institute</li>
                              <li>Chikushi Jogakuen University</li>
                              <li>Right Tsushin</li>
                              <li>Global Partners</li>
                              <li>Mahal.KitaQ</li>
                              <li>GTP</li>
                              <li>Yoko Cram school</li>
                              <li>Sophia club</li>
                              <li>IMAGINE</li>
                         </ul>
                    </div>
                    <div class="col-sm-4 bg-color">
                         <h3><u>Actual Results</u></h3>
                         <h5 style="font-size: 19px;">About <strong style="color: red;">10,000</strong> Lessons or more per day.</h5>
                    </div>
               </div>
               <div class="row">
                    <h2><u>B</u>&nbsp<i class="fa fa-caret-square-o-down" style="color: #29ca8e;"></i></h2>
                    <h3 class="text-center"><u>CORPORATION</u></h3>
                    <div class="col" style="margin-top: 5px;">
                         <!-- TABLE 2 -->
                         <table class="table table-striped table-bordered bg-color">
                           <thead>
                             <tr>
                               <th scope="col"></th>
                               <th scope="col">Globalization Model Plan</th>
                               <th scope="col">Benefits Plans</th>
                             </tr>
                           </thead>
                           <tbody>
                             <tr>
                               <th scope="row">Purpose</th>
                               <td>Improve the level of English proficiency of all the employees. </td>
                               <td>For benefits that employees and their families can use. (No expense as a company)</td>
                             </tr>
                             <tr>
                               <th scope="row">cost</th>
                               <td>Company Expenses</td>
                               <td>Self-pay (Special discount from regular price)</td>
                             </tr>
                             <tr>
                               <th scope="row">course</th>
                               <td>・ Use up 50 times for 5 months<br>・ Participation in group lessons, etc.</td>
                               <td>・ Unlimited use for 5 months, etc. <br>Please note:* Some plans are not applicable.</td>
                             </tr>
                           </tbody>
                         </table>
                    </div>
               </div>
               <div class="row">
                    <h2><u>Customer's Feedback</u>&nbsp<i class="fa fa-user" style="color: #29ca8e;"></i></h2>
                    <center>
                         <div class="row bg-color">
                              <div class="col-sm-1">
                                   <div class="circle"><strong>1</strong></div>
                              </div>
                              <div class="col-sm-11">
                                   <h3 class="cust-voice">Osaka Institute of Technology International Exchange Center</h3>
                              </div>
                         </div>
                    </center>
                    <!-- TABLE 3  -->
                    <table class="table table-striped table-bordered bg-color" style="margin-top: 5px;">
                      <thead>
                        <tr>
                          <th scope="col">Introduction</th>
                          <th scope="col">A few years ago, we established an international exchange program and have been working to develop global human resources in the world. As a step, we introduced  Philippine English language study abroad program with  a cheaper online study  as in-school credit training, aiming to change the mindset of the students thereafter.<br> * Actual class → 100 or more people in total</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <th scope="row">Package</th>
                          <td>・ 4 times a day (50 minutes per class) Mon-Fri with 1-4 weeks selection.<br>・ A dedicated and fixed curriculum is available. Free participation for group lessons are provided twice a week.<br>・ Entrance/Trial lessons and graduation ceremonies are held online. A level check test was conducted online before the class started.<br>・ The students are given certificate of completion after the study.</td>
                        </tr>
                        <tr>
                          <th scope="row">Class Feedback</th>
                          <td>The students were very satisfied with the class given. It is because the staff  always check each level and give lessons according to the student`s level. Also, apart from the usual online lessons, NILS created special Teacher Team for  Osaka Institute of Technology to check on their progress regularly.<br>Many Japanese staff were there to help and assist with the concern and with the coordination of the instructors in the Philippines.<br>Thanks to the fact that they created an environment where students can easily consult directly about classes, instructors, and how to study English in detail. It was helpful because there were few opinions and questions from the students on the International Exchange Center side.</td>
                        </tr>
                      </tbody>
                    </table>
                    <center>
                         <div class="row bg-color">
                              <div class="col-sm-1">
                                   <div class="circle"><strong>2</strong></div>
                              </div>
                              <div class="col-sm-11">
                                   <h3 class="cust-voice">Right Tsushin Group Co., Ltd.</h3>
                              </div>
                         </div>
                    </center>
                    <!-- TABLE 4 -->
                    <table class="table table-striped table-bordered bg-color" style="margin-top: 5px;">
                      <thead>
                        <tr>
                          <th scope="col">Opportunity Of Introduction</th>
                          <th scope="col">Since 2015, we have been accepting  students for internship  and providing English conversation lessons to expatriates at affiliated companies in Cebu. In order to develop global human resources in the future, we have decided to introduce online lessons as part of our welfare program for all new graduates and existing employees.</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <th scope="row">Package</th>
                          <td>Up to 3 group lessons per day are set up according to each level (50 minutes per class) from Monday to Friday.<br> Flexible schedule adjustment through  direct follow-up by our Japanese support.</td>
                        </tr>
                        <tr>
                          <th scope="row">Impressions Of Introduction</th>
                          <td>At the beginning, a trial lesson was set up for about 50-100 employees over 1-2 weeks, and it was easy for both the planner and the students to get an actual set-up and idea on how it will be.  Because the school specializes in one-on-one, it always gives each employee an opportunity to speak, and employees who were worried about communication can participate with peace of mind.<br> Group lessons are very small and can be up to 5 people, so it's easy to keep an eye on and feel at ease.</td>
                        </tr>
                      </tbody>
                    </table>
               </div>
          </div>
     </section>

     <!-- TRIAL LESSON MODAL -->
     <?php include 'triallesson_modal.php';?>

     <!-- FOOTER -->
     <?php include 'footer.php'?>

     <!--AJAX -->
     <script type="text/javascript">
          $( "#formid" ).submit(function( event ) {
               event.preventDefault();

                $.ajax({
                  url: 'sendtriallesson',
                  type: 'POST',
                  data:  $('#formid').serialize(),
                  success: function(response) { 
                  if(response == 'Success') {  
                      $('#exampleModal').html("Success");
                      $('#exampleModal').modal('show'); //twitter bootstrap modal  
                  },
               });

               });
     </script>

     <!-- JAVASCIPTS && SCRIPTS-->
     <?php include 'link_scripts.php'?>

</body>
</html>