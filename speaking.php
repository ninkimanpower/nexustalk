<?php include 'sendtriallesson.php';?>

<!DOCTYPE html>
<html lang="en">
<head>
	<title>NILS Online - Speaking</title>
	<?php include 'header.php'?>
</head>
<body data-spy="scroll" data-target=".navbar-collapse" data-offset="50">
     <!-- PRE LOADER -->
     <section class="preloader">
          <div class="spinner">

               <span class="spinner-rotate"></span>
               
          </div>
     </section>
     <!-- Navbar -->
     <?php include 'navbar.php';?>

     <!-- STICKY SOCIAL -->
     <?php include 'sticky_social.php';?>

     <!-- PARALLAX EFFECT -->
      <div class="parallax-speaking-upper"></div>

     <!-- SECTION START -->
<!--      <section> -->
      
     	<div class="container">
     		<div class="row">
     			<div class="col-md-12 col-sm-12">
                         <div class="section-title text-center">
                              <h2 class="callan-title" style="margin-top: 5px;">スピーキング<hr style="border: 1px solid black"><small class="smol">初心者にもおススメです。</small></h2>
                         </div>
                </div>   
     		</div>
     		<div class="row">
     			<p class="callan-text">生徒は、先生と会話したり、質問に答えたり、写真や与えられた状況についてコメントしたりして、話す練習をします。いくつかの活動は、学生にオーラルコミュニケーションスキルを練習する機会を与え、発音や語彙などの新しく習得したスキルを披露します。（インタビュー）</p>
     		</div>
     	</div>
      <div style="background-color: #e9ecef;">
      <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <div class="section-title text-center">
                    <h3 class="callan-subtitle">コース条件</h3>
                    <hr style="border: 1px solid black">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md">
              <p class="callan-text-second"><i class="fa fa-check-square-o fa-3x" aria-hidden="true"></i>英語の基礎知識と各種テストでのスコアアップに必要なスキルを身に着けられる。</p>
            </div>
            <div class="col-md">
              <p class="callan-text-second"><i class="fa fa-check-square-o fa-3x" aria-hidden="true"></i>全世代受講可能</p>
            </div>
        </div>
      </div>
      </div>

<!--      </section> -->


     <!-- TRIAL LESSON MODAL -->
     <?php include 'triallesson_modal.php';?>

     <!-- FOOTER -->
     <?php include 'footer.php'?>

     <!--AJAX -->
     <script type="text/javascript">
          $( "#formid" ).submit(function( event ) {
               event.preventDefault();

                $.ajax({
                  url: 'sendtriallesson',
                  type: 'POST',
                  data:  $('#formid').serialize(),
                  success: function(response) { 
                  if(response == 'Success') {  
                      $('#exampleModal').html("Success");
                      $('#exampleModal').modal('show'); //twitter bootstrap modal  
                  },
               });

               });
     </script>

     <!-- JAVASCIPTS && SCRIPTS -->
     <?php include 'link_scripts.php'?>

</body>
</html>