<?php include 'sendtriallesson.php'; ?>



<!DOCTYPE html>

<html>

<head>

     <title>NILS Online - Corporate/School Programs</title>

     <?php include 'header.php'?>

</head>

<body data-spy="scroll" data-target=".navbar-collapse" data-offset="50">



     <!-- PRE LOADER -->

     <section class="preloader">

          <div class="spinner">



               <span class="spinner-rotate"></span>

               

          </div>

     </section>





     <?php include 'navbar.php';?>



     <!-- STICKY SOCIAL -->

     <?php include 'sticky_social.php';?>

     

     <section id="courses" class="courses" >

          <div class="container">

               <h2>NILSオンラインはリーズナブルな価格で始まります。</h2>

               <hr>

               <div class="row">

                    <div class="col-md-6">

                         <img src="images/3.JPG" class="img-fluid img-thumbnail" style="max-width: 100%; height: auto;">

                    </div>

                    <div class="col-md-6 bg-color">

                        <h3>1ヵ月4,000円～</h3>

                        <p><i class="fa fa-check">（月4回のレッスン。1コマ25分の場合）</i></p>

                        <p><i class="fa fa-check">学校や企業に合わせた特別プランの作成などを行い、最大限の柔軟性を提供します。</i></p>

                        <div class="border-0">

                             <p>世界クラスの無料プリケーション「Skype」をダウンロードするだけです。  ⇄ </p>

                              <a href="https://www.skype.com/en/get-skype/" target="_blank"><img src="images/skype.png" class="img-thumbnail" style="max-width: 15%"></a><span>&#8592; *Click to Download Skype*</span><br>

                              <span>※予約サイトの無料登録が必要です。 各企業には管理番号が割り当てられます。</span>

                        </div>

                        <h4><u>NILSオンラインは簡単に始めることが出来ます。</u></h4>

                    </div>

               </div>

               <h2><u>A</u>&nbsp<i class="fa fa-caret-square-o-down" style="color: #29ca8e;"></i></h2>

               <div class="row">

                    <!-- TABLE 1 -->

                    <table class="table table-striped table-bordered bg-color">

                      <thead>

                        <tr>

                          <th scope="col"></th>

                          <th scope="col">正課授業・課外授業</th>

                          <th scope="col">おうち留学</th>

                          <th scope="col">海外留学</th>

                        </tr>

                      </thead>

                      <tbody>

                        <tr>

                          <th scope="row">目的</th>

                          <td>英語の新たな授業ツールとして、グループでのレッスンを行う。</td>

                          <td>長期休みを使用した、固定プログラムを行い、単位認定を認める。</td>

                          <td>長期休みを使用した、固定プログラムを行い、単位認

定を認める。

</td>

                        </tr>

                        <tr>

                          <th scope="row">費用</th>

                          <td>学校負担・個人負担 </td>

                          <td>一部学校負担・個人負担</td>

                          <td>個人負担（割引）</td>

                        </tr>

                        <tr>

                          <th scope="row">コース</th>

                          <td> ・ 5ヶ月間50コマ使い切り<br>・ 週2回グループレッスン<br>・ 10コマレッスンを単位認定。</td>

                          <td>・ 2週間1日4-6時間の固定レッスン。通常留学同様卒業証明書、成績表発行。</td>

                          <td>・ 1週間から可能 <br>・ 1日4-10コマ選択 <br>・ 渡航前にオンラインレッスンを無料で受講可能。</td>

                        </tr>

                      </tbody>

                    </table>

               </div>

               <hr>

               <div class="row">

                    <div class="col-sm-4">

                         <!-- <i class="fa fa-graduation-cap fa-10x" aria-hidden="true"></i> -->

                         <img src="images/school-icon.png" class="img-thumbnail" style="max-width: 90%;">

                    </div>

                    <div class="col-sm-4 bg-color">

                         <h3><u>Partnered Schools Who Have Taken Classes:</u></h3>   

                         <ul style="font-size: 15px;">

                              <li>徳山高等専門学校様</li>

                              <li>大阪工業大学国際交流センター様</li>

                              <li>神田外語学院様</li>

                              <li>梅光学院大学様</li>

                              <li>筑紫女学園大学様</li>

                              <li>グローバル教育学院様</li>

                              <li>ライト通信様</li>

                              <li>グローバルパートナーズ様など</li>

                              <li>学生団体Mahal.KitaQ 様</li>

                              <li>GTP 様</li>

                              <li>ヨコ塾様</li>

                              <li>ソフィア倶楽部様</li>

                              <li>英会話IMAGINE 様など</li>

                         </ul>

                    </div>

                    <div class="col-sm-4 bg-color">

                         <h3><u>導入実績　actual results</u></h3>

                         <h5 style="font-size: 19px;">1ヶ⽉でなんと約<strong style="color: red;">10,000</strong> レッスン以上実施</h5>

                    </div>

               </div>

               <div class="row">

                    <h2><u>B</u>&nbsp<i class="fa fa-caret-square-o-down" style="color: #29ca8e;"></i></h2>

                    <h3 class="text-center"><u>CORPORATION</u></h3>

                    <div class="col" style="margin-top: 5px;">

                         <!-- TABLE 2 -->

                         <table class="table table-striped table-bordered bg-color">

                           <thead>

                             <tr>

                               <th scope="col"></th>

                               <th scope="col">グローバル化モデルプラン</th>

                               <th scope="col">福利厚生プラン</th>

                             </tr>

                           </thead>

                           <tbody>

                             <tr>

                               <th scope="row">目的</th>

                               <td>社員全体の英語力の底上げ。自主性を養うためのツール。</td>

                               <td>社員とその家族が使用できる福利厚生に。

（企業様としての負担はなし）

</td>

                             </tr>

                             <tr>

                               <th scope="row">費用</th>

                               <td>会社負担</td>

                               <td>自己負担（特別割引）</td>

                             </tr>

                             <tr>

                               <th scope="row">コース</th>

                               <td>・ 5ヶ月間50コマ使い切り<br>・ 自由参加グループレッスン開設など</td>

                               <td>・5ヶ月間無制限使い放題など<br>※一部対象外のプランもあります。</td>

                             </tr>

                           </tbody>

                         </table>

                    </div>

               </div>

               <div class="row">

                    <h2><u>導入企業・学校様からの声</u>&nbsp<i class="fa fa-user" style="color: #29ca8e;"></i></h2>

                    <center>

                         <div class="row bg-color">

                              <div class="col-sm-1">

                                   <div class="circle"><strong>1</strong></div>

                              </div>

                              <div class="col-sm-11">

                                   <h3 class="cust-voice">学校法人常翔学園

大阪工業大学国際交流センター様

</h3>

                              </div>

                         </div>

                    </center>

                    <!-- TABLE 3  -->

                    <table class="table table-striped table-bordered bg-color" style="margin-top: 5px;">

                      <thead>

                        <tr>

                          <th scope="col">導入のきっかけ</th>

                          <th scope="col">数年前より国際交流プログラムを新設し、すべての学科において今後の世界におけるグローバル人材の育成に取り組んでいました。そのステップとして、単位内研修としてフィリピン語学留学や、より安価なオンライン留学を取り入れ、その後の生徒の意識改革を狙いました。<br> ※導入実績→延べ100名以上</th>

                        </tr>

                      </thead>

                      <tbody>

                        <tr>

                          <th scope="row">パッケージ</th>

                          <td>・ 1日4コマ（1コマ50分）月～金1-4週間の選択。<br>・ 専用カリキュラムをご用意。週2回の自由参加型グループレッスン提供、卒業証明書<br>・ 成績表配布。入学式<br>・ 卒業式をオンラインにて実施。授業開始前にWEBによるレベルチェックテスト実施。</td>

                        </tr>

                        <tr>

                          <th scope="row">導入をした感想</th>

                          <td>常に個々のレベルをチェックしながら、そこに水準を合わせたレッスンをしてくれたので、生徒の満足度が非常に高かった。また通常のオンラインレッスンとは別に大阪工業大学のTeacher Teamを作ってくれたため、都度講師が変わるようなこともなかった。<br>フィリピンにいる講師と一緒に、日本人スタッフが多数駐在していて、授業のこと・講師のこと<br>・英語の細かい勉強の仕方など、直接生徒が相談をしやすい環境を作ってくれたおかげで、国際交流センター側で生徒からの意見や質問などが少なく助かった。</td>

                        </tr>

                      </tbody>

                    </table>

                    <center>

                         <div class="row bg-color">

                              <div class="col-sm-1">

                                   <div class="circle"><strong>2</strong></div>

                              </div>

                              <div class="col-sm-11">

                                   <h3 class="cust-voice">株式会社ライト通信グループ様</h3>

                              </div>

                         </div>

                    </center>

                    <!-- TABLE 4 -->

                    <table class="table table-striped table-bordered bg-color" style="margin-top: 5px;">

                      <thead>

                        <tr>

                          <th scope="col">導入のきっかけ</th>

                          <th scope="col">2015年より、セブの関連会社においてインターン留学生の受け入れ・駐在員に対する英会話レッスンの提供を行ってきた。今後のグローバル人材の育成に取り組むべく、新卒内定者・既存社員全てを対象とした、福利厚生の一環として、オンラインレッスンの導入に踏み切った。</th>

                        </tr>

                      </thead>

                      <tbody>

                        <tr>

                          <th scope="row">パッケージ</th>

                          <td>各レベルに応じて1日最大3コマグループレッスンを設置（1コマ50分）月～金日本人サポーターによる直接フォローによりスケジュール調整をフレキシブルに対応。</td>

                        </tr>

                        <tr>

                          <th scope="row">導入をした感想</th>

                          <td>初めに50-100人ほどの社員に対して、1-2週間かけて体験レッスンを設けてくれ、企画者・受講者ともにイメージをつけやすかった。マンツーマンに特化した学校故に、社員一人ひとりに喋る機会を必ず与えてくれ、コミュニケーションを心配していた社員も安心して参加できている。

グループレッスンは超少人数制で最大5人までというのも、目が行き届きやすく安心できる。

</td>

                        </tr>

                      </tbody>

                    </table>

               </div>

          </div>

     </section>



     <!-- TRIAL LESSON MODAL -->

     <?php include 'triallesson_modal.php';?>



     <!-- FOOTER -->

     <?php include 'footer.php'?>



     <!--AJAX -->

     <script type="text/javascript">

          $( "#formid" ).submit(function( event ) {

               event.preventDefault();



                $.ajax({

                  url: 'sendtriallesson',

                  type: 'POST',

                  data:  $('#formid').serialize(),

                  success: function(response) { 

                  if(response == 'Success') {  

                      $('#exampleModal').html("Success");

                      $('#exampleModal').modal('show'); //twitter bootstrap modal  

                  },

               });



               });

     </script>



     <!-- JAVASCIPTS && SCRIPTS-->

     <?php include 'link_scripts.php'?>



</body>

</html>