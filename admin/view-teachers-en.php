<?php include 'navbar.php';?>



	<?php 

		$person_id = $_SESSION['person_id'];

		$sql = "SELECT * FROM person WHERE person_id = '$person_id'";



		$result= mysqli_query($databaseconnection,$sql);

		$data=mysqli_fetch_assoc($result);



		$string = $data['mname'];





		$firstCharacter = $string[0];



	?>

<main>

	<div class="container-fluid">



		<h1 class="mt-4">User : <small><?php echo $data['fname'];?>&nbsp<?php echo $firstCharacter;?>.&nbsp<?php echo $data['lname'];?></small></h1>

        <ol class="breadcrumb mb-4">

            <li class="breadcrumb-item active">Teachers</li>

        </ol>

        <!-- PRESS MODAL ADD TEACHER -->

        <a data-toggle="modal" data-target="#teacher_modal" class="btn btn-primary mb-3">Add Teacher</a>

	</div>

	<div class="container-fluid">

		<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">

			<thead>

				<tr>

				<th>Full Name</th>

				<th>Nickname</th>

				<th>Years</th>

				<th>Interests</th>

				<th>Comments</th>

				<th>Status</th>

				<th>Actions</th>

				</tr>

			</thead>

			<tbody>

		<?php

			$sql = "SELECT * FROM teacher_en";



			$fetch = $databaseconnection->query($sql);

			if ($outputresult = mysqli_query($databaseconnection, $sql)) {

				if (mysqli_num_rows($outputresult) > 0) {

			

			?>

			<?php

				while ($row = mysqli_fetch_array($outputresult)) {

			?>

			

				<tr>

				<th><?php echo $row['full_name_en'];?></th>

				<th><?php echo $row['nickname_en'];?></th>

				<th><?php echo $row['year_work_en'];?></th>

				<th><?php echo $row['interests_en'];?></th>

				<th><?php echo $row['comments_en'];?></th>

				<th>
      
          <?php echo $row['islocked_en'] ? "<a href='unlock-status?teacher_en_id={$row['teacher_en_id']}'><p class='btn btn-circle btn-danger'><i class='fa fa-lock'></i> Unlock &nbsp;&nbsp;</p></a>" : "<a href='lock-status?teacher_en_id={$row['teacher_en_id']}'><p class='btn btn-circle btn-success'><i class='fa fa-unlock'></i> Lock &nbsp;&nbsp;</p></a>"?>   

        </th>

				<th>

          <input type="button" name="view" value="Edit" id="<?php echo $row['teacher_en_id']; ?>" class="btn btn-primary view_data" data-target="#view_data_modal" data-toggle="modal">

          <?php

          if ($_SESSION['person_type'] == 'admin') {

          ?>

          <a href="delete-action?teacher_en_id=<?php echo $row['teacher_en_id'];?>" onclick="return confirm('Are you sure you want to Delete this Teacher?')" class="btn btn-warning mt-2">Delete</a>

          <?php

          }

          ?>

        </th>

				</tr>

			

			<?php

				}}else{

					?>



			

			<?php

				}

			}

		?>

		</tbody>

		</table>

	</div>

</main>







     <!--Add Teacher Modal -->

     <div class="modal fade" id="teacher_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">

       <div class="modal-dialog" role="document">

         <div class="modal-content">

           <div class="modal-header">

             <h3 class="modal-title" id="exampleModalLabel">Add New Teachers</h3>

           </div>

           <div class="modal-body">

               <form method="POST" action="add_teacher_en" enctype="multipart/form-data">

             	<div class="form-group">

                      <label for="name">Teacher Picture :</label>

                      <input type="file" name="image_link_en" class="form-control" required>

                 </div>

                 <div class="form-group">

                      <label for="name">Full Name :</label>

                      <input type="text" name="full_name_en" class="form-control"  aria-describedby="nameHelp" placeholder="Name" required>

                 </div>

                 <div class="form-group">

                 	<div class="row">

                 		<div class="col-sm-6">

                 			 <label for="nickname">Nickname :</label>

                 			 <input type="text" name="nickname_en" class="form-control"  placeholder="Nickname" required>

                 		</div>

                 		<div class="col-sm-6">

                 			 <label for="year_work">Years in Nils :</label>

                 			 <input type="text" name="year_work_en" class="form-control" placeholder="Work Experience">

                 		</div>

                 	</div>

                 </div>

                 <div class="form-group">

                 	<label for="interests">Interests :</label>

                 	<!-- <input type="text" name="interests" class="form-control" placeholder="Interests"> -->

                    <textarea  type="text" name="interests_en" class="form-control" placeholder="Interests"></textarea>

                 </div>

                 <div class="form-group">

                 	<label for="comments">Comments/Sayings :</label>

                 	<!-- <input type="text" name="comments" class="form-control"  placeholder="Comments / Sayings"> -->

                  <textarea type="text" name="comments_en" class="form-control"  placeholder="Comments / Sayings"></textarea>

                 </div>

                 <div class="modal-footer">

                      <input type="submit" name="submit" id="submit" value="submit" class="btn btn-success btn-sm" style="font-size: 20px;">

                 </div>

               </form>

           </div>

         </div>

       </div>

     </div>  





     <!-- VIEW AND UPDATE TEACHER MODAL -->



     <div class="modal fade" id="view_data_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">

       <div class="modal-dialog" role="document">

         <div class="modal-content">

           <div class="modal-header">

             <h3 class="modal-title" id="exampleModalLabel">Update Teachers</h3>

           </div>

           <div class="modal-body">

           	<div class="container">

               <form method="POST" id="insert_form" enctype="multipart/form-data">

             	<div class="form-group">

                      <label for="name">Teacher Picture :</label>

                      <input type="file" id="image_link_en" name="image_link_en" class="form-control" >

                 </div>

                 <div class="form-group">

                      <label for="name">Full Name :</label>

                      <input type="text" name="full_name_en"  id="full_name_en" class="form-control">

                 </div>

                 <div class="form-group">

                 	<div class="row">

                 		<div class="col-sm-6">

                 			 <label for="nickname">Nickname :</label>

                 			 <input type="text" name="nickname_en"  id="nickname_en" class="form-control">

                 		</div>

                 		<div class="col-sm-6">

                 			 <label for="year_work">Years in Nils :</label>

                 			 <input type="text" name="year_work_en"  id="year_work_en" class="form-control">

                 		</div>

                 	</div>

                 </div>

                 <div class="form-group">

                 	<label for="interests">Interests :</label>

                 	<!-- <input type="text" name="interests"  id="interests" class="form-control"> -->

                  <textarea  type="text" name="interests_en"  id="interests_en" class="form-control"></textarea>

                 </div>

                 <div class="form-group">

                 	<label for="comments">Comments/Sayings :</label>

                 	<!-- <input type="text" name="comments"  id="comments" class="form-control"> -->

                  <textarea type="text" name="comments_en"  id="comments_en" class="form-control"></textarea>

                 	<input type="hidden" name="teacher_en_id" id="teacher_en_id">

                 	<input type="hidden" name="islocked_en" id="islocked_en">

                 </div>

                 <div class="modal-footer">

                     <!--  <input type="submit" name="submit" id="submit" value="submit" class="btn btn-success btn-sm" style="font-size: 20px;"> -->

                      <input type="submit" name="submit" id="submit" value="Update" class="btn btn-success btn-sm" style="font-size: 20px;">

                 </div>

               </form>

               </div>

           </div>

         </div>

       </div>

     </div>  









<script>

		$(document).ready(function(){



		 function submitForm(action)

		    {

		        document.getElementById('insert_form').action = action;

		        document.getElementById('insert_form').submit();

		    }

		$(document).on('click', '.view_data', function(){

			var teacher_en_id = $(this).attr("id");

			$.ajax({

				url:"view-teacher-en-process",

				method:"POST",

				data:{teacher_en_id:teacher_en_id},

				dataType:"json",

				success:function(data){

					$('#teacher_en_id').val(data.teacher_en_id);

					$('#full_name_en').val(data.full_name_en);

					$('#nickname_en').val(data.nickname_en);

					$('#year_work_en').val(data.year_work_en);

					$('#interests_en').val(data.interests_en);

					$('#comments_en').val(data.comments_en);

					$('#image_link_en').val(data.teacher_photo_en);

					$('#islocked_en').val(data.islocked_en);

					$('#insert').val("Update");

          $('#view_data_modal').modal('show');



				}

			});

		});





		$('#insert_form').on("submit", function(event){  

  

                $.ajax({  

                	 enctype:"multipart/form-data",

                     url:"update-teacher-en",  

                     method:"POST",  

                     data:$('#insert_form').serialize(),

                    beforeSend:function(){  

                          $('#submit').val("Updating");  

                     },    

                     success:function(data){  

                          $('#insert_form')[0].reset();  

                          $('#view_data_modal').modal('hide');

                          $('#teacher_en_id').val('');

                          $('#full_name_en').val('');

                          $('#nickname_en').val(''); 

                          $('#year_work_en').val('');

                          $('#interests_en').val('');  

                          $('#comments_en').val('');

                          $('#image_link_en').val(''); 

                          $('#islocked_en').val('');    

                          // $('#new_table').html(data);





                     }  



                });  

             

      });  



	});



</script>