<?php
	session_start();

	require_once 'dbconnection.php';
	// REVIEW ROW JAPANESE
	if ($_GET['review_id']) {
		$idtodelete = $_GET['review_id'];

		$sql = "DELETE FROM reviews WHERE review_id = $idtodelete";


		if ($databaseconnection->query($sql) === TRUE) {
			header('Location: ./?page_id=view-reviews');
		}else{
			echo "<script>alert('Error On Deleting Review')</script>";
			header('Location: ./?page_id=view-reviews');
		}
	// REVIEW ROW ENGLISH
	}elseif ($_GET['review_en_id']) {
		$idtodelete = $_GET['review_en_id'];

		$sql = "DELETE FROM review_en WHERE review_en_id = $idtodelete";


		if ($databaseconnection->query($sql) === TRUE) {
			header('Location: ./?page_id=view-reviews-en');
		}else{
			echo "<script>alert('Error On Deleting Review')</script>";
			header('Location: ./?page_id=view-reviews-en');
		}

	// TEACHER ROW ENGLISH
	}elseif ($_GET['teacher_en_id']) {

		$idtodelete = $_GET['teacher_en_id'];

		$query = "SELECT * FROM teacher_en WHERE teacher_en_id = $idtodelete";

		$result= mysqli_query($databaseconnection,$query);

		$data=mysqli_fetch_assoc($result);

		$image = $data['teacher_photo_en'];

		$fileloc = '../uploads/'.$image;

		unlink($fileloc);

		$sql = "DELETE FROM teacher_en WHERE teacher_en_id = $idtodelete";


		if ($databaseconnection->query($sql) === TRUE) {
			header('Location: ./?page_id=view-teachers-en');
		}else{
			echo "<script>alert('Error On Deleting Teacher')</script>";
			header('Location: ./?page_id=view-teachers-en');
		}


	// TEACHER ROW JAPANESE
	}elseif ($_GET['teacher_id']) {

		$idtodelete = $_GET['teacher_id'];

		$query = "SELECT * FROM teacher WHERE teacher_id = $idtodelete";

		$result= mysqli_query($databaseconnection,$query);

		$data=mysqli_fetch_assoc($result);

		$image = $data['teacher_photo'];

		$fileloc = '../uploads/'.$image;

		unlink($fileloc);

		$sql = "DELETE FROM teacher WHERE teacher_id = $idtodelete";


		if ($databaseconnection->query($sql) === TRUE) {
			header('Location: ./?page_id=view-teachers');
		}else{
			echo "<script>alert('Error On Deleting Teacher')</script>";
			header('Location: ./?page_id=view-teachers');
		}
	}


?>
