<?php include 'navbar.php';?>



	<?php 

		$person_id = $_SESSION['person_id'];

		$sql = "SELECT * FROM person WHERE person_id = '$person_id'";



		$result= mysqli_query($databaseconnection,$sql);

		$data=mysqli_fetch_assoc($result);



		$string = $data['mname'];





		$firstCharacter = $string[0];



	?>

<main>

	<div class="container-fluid">



		<h1 class="mt-4">User : <small><?php echo $data['fname'];?>&nbsp<?php echo $firstCharacter;?>.&nbsp<?php echo $data['lname'];?></small></h1>

        <ol class="breadcrumb mb-4">

            <li class="breadcrumb-item active">Student Reviews</li>

        </ol>

        <!-- PRESS MODAL ADD REVIEWS -->

        <a data-toggle="modal" data-target="#review_modal" class="btn btn-primary mb-3">Add Reviews</a>

	</div>



	<div class="container-fluid">

		<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">

			<thead>

				<tr>

					<th>Initial Name</th>

					<th>Age</th>

					<th>Review/Comments</th>

					<th>Ratings(1~5)</th>

					<th>Status</th>

					<th>Actions</th>

				</tr>

			</thead>

		<tbody>

		<?php

			$sql = "SELECT * FROM review_en";



			$fetch = $databaseconnection->query($sql);

			if ($outputresult = mysqli_query($databaseconnection, $sql)) {

				if (mysqli_num_rows($outputresult) > 0) {

			

			?>

			<?php

				while ($row = mysqli_fetch_array($outputresult)) {

			?>

			

				<tr>

				<th><?php echo $row['initial_en'];?></th>

				<th><?php echo $row['age_en'];?></th>

				<th><?php echo $row['review_comment_en'];?></th>

				<th><?php echo $row['star_number_en'];?></th>

				<th>

            <?php echo $row['islocked_en'] ? "<a href='unlock-status?review_en_id={$row['review_en_id']}'><p class='btn btn-circle btn-danger'><i class='fa fa-lock'></i> Unlock &nbsp;&nbsp;</p></a>" : "<a href='lock-status?review_en_id={$row['review_en_id']}'><p class='btn btn-circle btn-success'><i class='fa fa-unlock'></i> Lock &nbsp;&nbsp;</p></a>"?>    

        </th>

				<th>

          <input type="button" name="view" value="Edit" id="<?php echo $row['review_en_id']; ?>" class="btn btn-primary view_data" data-target="#update_review" data-toggle="modal">

          <?php

          if ($_SESSION['person_type'] == 'admin') {

          ?>

          <a href="delete-action?review_en_id=<?php echo $row['review_en_id'];?>" onclick="return confirm('Are you sure you want to Delete this Review?')" class="btn btn-warning mt-2">Delete</a>

          <?php

          }

          ?>

        </th>

				</tr>

			

			<?php

				}}else{

					?>



			

			<?php

				}

			}

		?>

		</tbody>

		</table>

	</div>

</main>





	<!-- ADD REVIEW MODAL -->

     <div class="modal fade" id="review_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">

       <div class="modal-dialog" role="document">

         <div class="modal-content">

           <div class="modal-header">

             <h3 class="modal-title" id="exampleModalLabel">Add New Reviews</h3>

           </div>

           <div class="modal-body">

               <form method="POST" action="add_reviews_en" enctype="multipart/form-data">

                 <div class="form-group">

                      <label for="name">Initial Name :</label>

                      <input type="text" name="initial_en" class="form-control"  aria-describedby="nameHelp" placeholder="Initials" required>

                 </div>

                 <div class="form-group">

                 	<div class="row">

                 		<div class="col-sm-6">

                 			 <label for="nickname">Age :</label>

                 			 <input type="text" name="age_en" class="form-control"  placeholder="Age" required>

                 		</div>

                 		<div class="col-sm-6">

                 			 <label for="year_work">Ratings(1~5/.5) :</label>

                 			 <input type="text" name="star_number_en" class="form-control" placeholder="Ratings">

                 		</div>

                 	</div>

                 </div>

                 <div class="form-group">

                 	<label for="comments">Comments/Review :</label>

                 	<textarea name="review_comment_en" class="form-control"  placeholder="Comments / Ratings"></textarea>

                 </div>

                 <div class="modal-footer">

                      <input type="submit" name="submit" id="submit" value="submit" class="btn btn-success btn-sm" style="font-size: 20px;">

                 </div>

               </form>

           </div>

         </div>

       </div>

     </div> 











	<!-- Edit/ UPDATE REVIEW MODAL -->

     <div class="modal fade" id="update_review" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">

       <div class="modal-dialog" role="document">

         <div class="modal-content">

           <div class="modal-header">

             <h3 class="modal-title" id="exampleModalLabel">Add New Reviews</h3>

           </div>

           <div class="modal-body">

               <form method="POST" id="insert_form" enctype="multipart/form-data">

                 <div class="form-group">

                      <label for="name">Initial Name :</label>

                      <input type="text" name="initial_en" id="initial_en" class="form-control"  aria-describedby="nameHelp" placeholder="Initials" required>

                 </div>

                 <div class="form-group">

                 	<div class="row">

                 		<div class="col-sm-6">

                 			 <label for="nickname">Age :</label>

                 			 <input type="text" name="age_en" id="age_en" class="form-control"  placeholder="Age" required>

                 		</div>

                 		<div class="col-sm-6">

                 			 <label for="year_work">Ratings(1~5/.5) :</label>

                 			 <input type="text" name="star_number_en" id="star_number_en" class="form-control" placeholder="Ratings">

                 		</div>

                 	</div>

                 </div>

                 <div class="form-group">

                 	<label for="comments">Comments/Review :</label>

                 	<textarea name="review_comment_en" id="review_comment_en" class="form-control"  placeholder="Comments / Reviews"></textarea>

                 	<input type="hidden" name="review_en_id" id="review_en_id">

                 </div>

                 <div class="modal-footer">

                      <input onclick="return confirm('Are you sure you want to Edit this Review?')" type="submit" name="submit" id="submit" value="Update" class="btn btn-success btn-sm" style="font-size: 20px;">

                 </div>

               </form>

           </div>

         </div>

       </div>

     </div> 











<script>

		$(document).ready(function(){



		 function submitForm(action)

		    {

		        document.getElementById('insert_form').action = action;

		        document.getElementById('insert_form').submit();

		    }

		$(document).on('click', '.view_data', function(){

			var review_en_id = $(this).attr("id");

			$.ajax({

				url:"view-reviews-en-process",

				method:"POST",

				data:{review_en_id:review_en_id},

				dataType:"json",

				success:function(data){

					$('#review_en_id').val(data.review_en_id);

					$('#initial_en').val(data.initial_en);

					$('#age_en').val(data.age_en);

					$('#review_comment_en').val(data.review_comment_en);

					$('#star_number_en').val(data.star_number_en);

					$('#insert').val("Update");

          $('#review_modal').modal('show');



				}

			});

		});





		$('#insert_form').on("submit", function(event){  

  

                $.ajax({  

                	 enctype:"multipart/form-data",

                     url:"update-reviews-en",  

                     method:"POST",  

                     data:$('#insert_form').serialize(),

                    beforeSend:function(){  

                          $('#submit').val("Updating");  

                     },    

                     success:function(data){  

                          $('#insert_form')[0].reset();  

                          $('#update_review').modal('hide');

                          $('#review_en_id').val('');

                          $('#initial_en').val('');

                          $('#age_en').val(''); 

                          $('#review_comment_en').val('');

                          $('#star_number_en').val('');    

                     }  



                });  

             

      });  



	});



</script>