<?php include 
    'navbar.php'; 

    if ($_SESSION['person_type'] != 'admin') {
        ?>
        <meta http-equiv="refresh" content="0;URL='index'" /> 
        <?php
        session_destroy();
    }
?>


                
                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-lg-7">
                                <div class="card shadow-lg border-0 rounded-lg mt-5">
                                    <div class="card-header"><h3 class="text-center font-weight-light my-4">Create Admin/Staff Account</h3></div>
                                    <div class="card-body">
                                        <form method="POST" action="register-process"  enctype="multipart/form-data">
                                            <div class="form-row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="small mb-1" for="inputFirstName">First Name</label>
                                                        <input class="form-control py-4" name="fname" id="inputFirstName" type="text" placeholder="Enter first name" required/>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="small mb-1" for="inputmiddleName">Middle Name</label>
                                                        <input class="form-control py-4" name="mname" id="inputmiddleName" type="text" placeholder="Enter Middle name" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="small mb-1" for="inputLastName">Last Name</label>
                                                <input class="form-control py-4" name="lname" id="inputLastName" type="text" placeholder="Enter last name" required/>
                                            </div>
                                            <div class="form-group">
                                                <label class="small mb-1" for="inputaccountType">Account Type</label>
                                                <select name="person_type" class="form-control" id="inputaccountType">
                                                    <option value="staff">Staff</option>
                                                    <option value="admin">Admin</option>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label class="small mb-1" for="inputEmailAddress">Email</label>
                                                <input class="form-control py-4" name="email" id="inputEmailAddress" type="email" aria-describedby="emailHelp" placeholder="Enter email address" required/>
                                            </div>
                                            <div class="form-row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="small mb-1" for="inputPassword">Password</label>
                                                        <input class="form-control py-4" id="inputPassword" name="password" type="password" placeholder="Enter password" required autofocus/>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="small mb-1" for="inputConfirm">Confirm Password</label>
                                                        <input class="form-control py-4" id="inputConfirm" name="cpassword" type="password" placeholder="Confirm password" required autofocus/>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- <div class="form-group mt-4 mb-0"><a class="btn btn-primary btn-block" name="register" href="register-process">Create Account</a></div> -->
                                            <!-- <div class="form-group mt-4 mb-0"><button type="button" class="btn btn-primary btn-block" name="register">Create Account</button></div> -->
                                            <center>
                                            <button class="btn-join btn-lg btn-primary text-uppercase" name="register">Create Account</button>
                                            </center>
                                        </form>
                                    </div>
<!--                                     <div class="card-footer text-center">
                                        <div class="small"><a href="login.html">Have an account? Go to login</a></div>
                                    </div> -->
                                </div>
                            </div>
                        </div>
                    </div>
                


   <script type="text/javascript">
       var inputPassword = document.getElementById("inputPassword")
          , inputConfirm = document.getElementById("inputConfirm");

        function validatePassword(){
          if(inputPassword.value != inputConfirm.value) {
            inputConfirm.setCustomValidity("Passwords Don't Match");
          } else {
            inputConfirm.setCustomValidity('');
          }
        }

        inputPassword.onchange = validatePassword;
        inputConfirm.onkeyup = validatePassword;
   </script>