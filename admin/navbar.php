<?php



if ($_SESSION['person_type'] != 'admin' && $_SESSION['person_type'] != 'staff') {

    include('dbconnection.php'); 

?>

<meta http-equiv="refresh" content="0;URL='index'" /> 

<?php

session_destroy();

}else{

?>



        <nav class="sb-topnav navbar navbar-expand navbar-dark bg-dark">



        	<?php

        		if ($_SESSION['person_type'] == 'admin') {

        			?>

        			<a class="navbar-brand" href="./?page_id=dashboard">Nils Online - Admin</a>

        		<?php

        		}else{

        		?>

        		<a class="navbar-brand" href="./?page_id=dashboard">Nils Online - Staff</a>

        		<?php

        		}

        	?>

            <button class="btn btn-link btn-sm order-1 order-lg-0" id="sidebarToggle" href="#"><i class="fas fa-bars"></i></button>

            <!-- Navbar Search-->

            <form class="d-none d-md-inline-block form-inline ml-auto mr-0 mr-md-3 my-2 my-md-0">

                <div class="input-group">

                    <input class="form-control" type="text" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2" />

                    <div class="input-group-append">

                        <button class="btn btn-primary" type="button"><i class="fas fa-search"></i></button>

                    </div>

                </div>

            </form>

            <!-- Navbar-->

            <ul class="navbar-nav ml-auto ml-md-0">

                <li class="nav-item dropdown">

                    <a class="nav-link dropdown-toggle" id="userDropdown" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-user fa-fw"></i></a>

                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">

                        <a class="dropdown-item" href="#">Settings</a>

                        <a class="dropdown-item" href="#">Activity Log</a>

                        <div class="dropdown-divider"></div>

                        <a class="dropdown-item" href="logout">Logout</a>

                    </div>

                </li>

            </ul>

        </nav>

                <div id="layoutSidenav">

            <div id="layoutSidenav_nav">

                <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">

                    <div class="sb-sidenav-menu">

                        <div class="nav">

                            <div class="sb-sidenav-menu-heading">Core</div>

                            <a class="nav-link" href="./?page_id=dashboard">

                                <div class="sb-nav-link-icon"><i class="fas fa-tachometer-alt"></i></div>

                                Dashboard

                            </a>

                            <div class="sb-sidenav-menu-heading">Interface</div>

                            <!-- Japanese Tab -->

                            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseLayouts" aria-expanded="false" aria-controls="collapseLayouts">

                                <div class="sb-nav-link-icon"><i class="fas fa-columns"></i></div>

                                Actions Japanese

                                <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>

                            </a>

                            <div class="collapse" id="collapseLayouts" aria-labelledby="headingOne" data-parent="#sidenavAccordion">

                                <nav class="sb-sidenav-menu-nested nav">

                                    <a class="nav-link" href="./?page_id=view-teachers">Teachers JP</a>

                                    <a class="nav-link" href="./?page_id=view-reviews">Reviews JP</a>

                                </nav>

                            </div>

                            <!-- English Tab -->

                             <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseEnglish" aria-expanded="false" aria-controls="collapseEnglish">

                                <div class="sb-nav-link-icon"><i class="fas fa-columns"></i></div>

                                Actions English

                                <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>

                            </a>

                            <div class="collapse" id="collapseEnglish" aria-labelledby="headingOne" data-parent="#sidenavAccordion">

                                <nav class="sb-sidenav-menu-nested nav">

                                    <a class="nav-link" href="./?page_id=view-teachers-en">Teachers EN</a>

                                    <a class="nav-link" href="./?page_id=view-reviews-en">Reviews EN</a>

                                </nav>

                            </div>



                            <?php

                                if ($_SESSION['person_type'] == 'admin') {

                            ?>

                            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseNew" aria-expanded="false" aria-controls="collapseNew">

                                <div class="sb-nav-link-icon"><i class="fas fa-columns"></i></div>

                                Accounts

                                <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>

                            </a>

                            <div class="collapse" id="collapseNew" aria-labelledby="headingOne" data-parent="#sidenavAccordion">

                                <nav class="sb-sidenav-menu-nested nav">

                                    <a class="nav-link" href="./?page_id=register">Create Account</a>

                                    <a class="nav-link" href="./?page_id=view-staff">View Staff</a>

                                </nav>

                            </div>

                            <?php

                                 }

                            ?>

<!--                             <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePages" aria-expanded="false" aria-controls="collapsePages">

                                <div class="sb-nav-link-icon"><i class="fas fa-book-open"></i></div>

                                Pages

                                <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>

                            </a> -->

<!--                             <div class="collapse" id="collapsePages" aria-labelledby="headingTwo" data-parent="#sidenavAccordion">

                                <nav class="sb-sidenav-menu-nested nav accordion" id="sidenavAccordionPages">

                                    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#pagesCollapseAuth" aria-expanded="false" aria-controls="pagesCollapseAuth">

                                        Authentication

                                        <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>

                                    </a>

                                    <div class="collapse" id="pagesCollapseAuth" aria-labelledby="headingOne" data-parent="#sidenavAccordionPages">

                                        <nav class="sb-sidenav-menu-nested nav">

                                            <a class="nav-link" href="login.html">Login</a>

                                            <a class="nav-link" href="register.html">Register</a>

                                            <a class="nav-link" href="password.html">Forgot Password</a>

                                        </nav>

                                    </div>

                                    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#pagesCollapseError" aria-expanded="false" aria-controls="pagesCollapseError">

                                        Error

                                        <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>

                                    </a>

                                    <div class="collapse" id="pagesCollapseError" aria-labelledby="headingOne" data-parent="#sidenavAccordionPages">

                                        <nav class="sb-sidenav-menu-nested nav">

                                            <a class="nav-link" href="401.html">401 Page</a>

                                            <a class="nav-link" href="404.html">404 Page</a>

                                            <a class="nav-link" href="500.html">500 Page</a>

                                        </nav>

                                    </div>

                                </nav>

                            </div> -->

                            <!-- <div class="sb-sidenav-menu-heading">Addons</div>

                            <a class="nav-link" href="charts.html">

                                <div class="sb-nav-link-icon"><i class="fas fa-chart-area"></i></div>

                                Charts

                            </a>

                            <a class="nav-link" href="tables.html">

                                <div class="sb-nav-link-icon"><i class="fas fa-table"></i></div>

                                Tables

                            </a> -->

                        </div>

                    </div>

                    <div class="sb-sidenav-footer">

                        <div class="small">Logged in as:</div>

                       <?php echo ucfirst($_SESSION['person_type']);?>

                    </div>

                </nav>

            </div>

            <div id="layoutSidenav_content">

<?php

}

?>