	$(document).ready(function(){

		 function submitForm(action)
		    {
		        document.getElementById('insert_form').action = action;
		        document.getElementById('insert_form').submit();
		    }
		$(document).on('click', '.view_data', function(){
			var teacher_id = $(this).attr("id");
			$.ajax({
				url:"view-teacher-process",
				method:"POST",
				data:{teacher_id:teacher_id},
				dataType:"json",
				success:function(data){
					$('#teacher_id').val(data.teacher_id);
					$('#name').val(data.name);
					$('#full_name').val(data.full_name);
					$('#nickname').val(data.nickname);
					$('#year_work').val(data.year_work);
					$('#interests').val(data.interests);
					$('#comments').val(data.comments);
					$('#submit').val("submit");
              		$('#view_data_modal').modal('show');

				}
			});
		});


		$('#insert_form').on("submit", function(event){  
  
                $.ajax({  
                     url:"update-teacher",  
                     method:"POST",  
                     data:$('#insert_form').serialize(),
                    beforeSend:function(){  
                          $('#submit').val("Updating");  
                     },    
                     success:function(data){  
                          $('#insert_form')[0].reset();  
                          $('#teacher_modal').modal('hide');
                          $('#teacher_id').val('');
                          $('#full_name').val('');
                          $('#nickname').val(''); 
                          $('#year_work').val('');
                          $('#interests').val('');  
                          $('#comments').val('');      
                          $('#new_table').html(data);


                     }  

                });  
             
      });  

	});
