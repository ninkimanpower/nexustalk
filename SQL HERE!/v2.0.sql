-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 29, 2021 at 11:21 AM
-- Server version: 10.4.18-MariaDB
-- PHP Version: 8.0.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `nils`
--

-- --------------------------------------------------------

--
-- Table structure for table `person`
--

CREATE TABLE `person` (
  `person_id` int(11) NOT NULL,
  `person_type` enum('admin','staff') NOT NULL,
  `fname` varchar(255) NOT NULL,
  `mname` varchar(255) NOT NULL,
  `lname` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) CHARACTER SET latin1 COLLATE latin1_general_cs NOT NULL,
  `islocked` tinyint(4) NOT NULL,
  `createdby` int(11) NOT NULL,
  `created_date` date NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `person`
--

INSERT INTO `person` (`person_id`, `person_type`, `fname`, `mname`, `lname`, `email`, `password`, `islocked`, `createdby`, `created_date`) VALUES
(1, 'admin', 'Prince Claire Joshua', 'Cabanlas', 'Caino', 'classicjosh37@gmail.com', 'password1234', 0, 0, '2021-03-04'),
(2, 'staff', 'Chelu', 'Arbs', 'Arbuis', 'arbuis@gmail.com', 'password1234', 0, 0, '2021-03-04'),
(4, 'staff', 'Hiroki', '', 'Iwabuchi', 'sample@gmail.com', 'Password1234', 0, 1, '2021-03-04'),
(5, 'staff', 'Rino', '', 'Yoshita', 'yoshita@gmail.com', 'password1234', 0, 1, '2021-03-05');

-- --------------------------------------------------------

--
-- Table structure for table `reviews`
--

CREATE TABLE `reviews` (
  `review_id` int(11) NOT NULL,
  `initial` varchar(255) NOT NULL,
  `age` varchar(255) NOT NULL,
  `review_comment` varchar(255) NOT NULL,
  `star_number` varchar(5) NOT NULL,
  `islocked` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `reviews`
--

INSERT INTO `reviews` (`review_id`, `initial`, `age`, `review_comment`, `star_number`, `islocked`) VALUES
(1, 'M.K', '40代', '私はいつもレッスンを受けるのが好きです。 仕事の合間に授業を受けますが、どんなに疲れていても先生の笑顔が癒してくれます。', '4.5', 0),
(2, 'K.I and N.I', 'キッズ', '最初、子供たちは英語が苦手でした。 授業で先生方に会えるのを楽しみにしています。また、留学中に先生方に直接会えるのも楽しみです。', '4.5', 0),
(3, 'K.T', '50代', '先生だけでなく、いつもアレンジをしてくれる日本人スタッフも丁寧でした。 語学学校が運営しているので、サポートが充実しています。', '4.5', 0),
(4, 'N.M', '20代', '先生からの質問に答えると、すごく興味を持ってくれてうれしかったです。 遅くまでレッスンが受けられるのもいいですね。', '5', 0);

-- --------------------------------------------------------

--
-- Table structure for table `review_en`
--

CREATE TABLE `review_en` (
  `review_en_id` int(11) NOT NULL,
  `initial_en` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `age_en` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `review_comment_en` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `star_number_en` varchar(5) CHARACTER SET utf8mb4 NOT NULL,
  `islocked_en` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `review_en`
--

INSERT INTO `review_en` (`review_en_id`, `initial_en`, `age_en`, `review_comment_en`, `star_number_en`, `islocked_en`) VALUES
(1, 'M.K', '40s', 'I always enjoy taking lessons. I take classes between jobs, but no matter how tired I am, the teacher\'s smile will heal me.', '4.5', 0),
(2, 'K.I and N.I ', 'kids', 'At first, children were not good at English. They\'re looking forward to seeing the teachers in the lesson And also looking forward to seeing the teachers in person while studying abroad.', '4.5', 0),
(3, 'K.T ', '50s', 'Not only the teacher but also the Japanese staff who always make arrangements were polite. Since it is run by a language school, the support is detailed. ', '4.5', 0),
(4, 'N.M', '20s', 'When I answered the question from the teacher, I was glad that he asked me a deeper question. It is also good to be able to take lessons until late.', '5', 0);

-- --------------------------------------------------------

--
-- Table structure for table `teacher`
--

CREATE TABLE `teacher` (
  `teacher_id` int(11) NOT NULL,
  `full_name` varchar(255) NOT NULL,
  `nickname` varchar(255) NOT NULL,
  `year_work` varchar(255) NOT NULL,
  `interests` text NOT NULL,
  `comments` text NOT NULL,
  `teacher_photo` text NOT NULL,
  `islocked` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `teacher`
--

INSERT INTO `teacher` (`teacher_id`, `full_name`, `nickname`, `year_work`, `interests`, `comments`, `teacher_photo`, `islocked`) VALUES
(10, 'Karen Mae Deniega', ' カレン', '6年', '映画やドラマ鑑賞、体を動かすこと', 'みなさん、こんにちは。英語を学ぶのをやめないでください。私たちと一緒に勉強してください。楽しく学び、忍耐強く勉強することで、間違いなく目標を達成できます。', '4bed2716ec1a99d290682181277141a6.jpeg', 0),
(11, 'Clarisse Ann Lopez', 'クレア', '7年', '雑誌を読む。旅行、買い物、映画鑑賞', '老若男女問わず、言語を学ぶのに遅いも早いもありません。今すぐやりましょう！きっとそれは今年の中の最高の決断になるでしょう。', '48b1237c9e68305cc850f2694b4453d0.jpeg', 0),
(12, 'Ariel Cabillo', 'イェン', '5年', '歌うこと、踊ること、そして食べること！', '英語を学んで楽しみましょう！お待ちしております！', '00e467366e3e6b2201df17b947539012.jpeg', 0),
(13, 'Vanjoe Cabanluna', 'ヴァンちゃん', '6年', '歌うこと、踊ること、料理、ボランティア活動', '今日1日を最も大事にして勉強して、自分を追い込んでください。できる限りのことをすることであなたの夢が得られるでしょう。', 'def305c922c93e3a3bc7b37718a3bae1.jpeg', 0),
(14, 'Vincent Vallera', 'ヤス', '2年', 'ホラー映画を見る、1人で歌うこと、ネットサーフィン', '英語を学ぶことは楽しいですよ！私を信じて！なぜならわたしは先生だから！', '473cf328ee93ae103adbf32d6f4c52ba.jpeg', 0),
(15, 'Lady Rosalie Yap', 'レディ', '2年', 'キリスト教の信仰、アニメ・韓国ドラマを見る', '英語を学ぶことは継続することが大切です。すぐにはよくならないです。継続的な学習プロセスを得ましょう。', '097d60e40f9af6f38420dd69de6f7408.jpeg', 0),
(16, 'Mary Joy Letiado', 'ジョイ', '2年', '映画鑑賞、音楽鑑賞、バドミントン', 'わたしたちはあなたのスキルを形作るために、モデル化しあなたに教えます。それを学び、楽しむことを忘れないでください。', '97775c91e7f9393cf9e1b49f17c08e99.jpeg', 0),
(17, 'Maricel Canete', 'マーズ', '4年', '料理、睡眠、食事、映画鑑賞', '夢を持ち続けてください。そして達成させるための勉強を続けてください。自身は結果につながります。楽しく学んでください！', 'dab93faf0a3c21f320f63a3c10946392.jpeg', 0),
(18, ' Lito Mahinay', 'リッツ', '5年', '筋トレ', '楽しく学び、決してあきらめないでください！', '58b3c47d7cb47518469de78075579f33.jpeg', 0),
(19, 'Ma. Teresa Bacalan', 'テレ', '2年', '歌、音楽鑑賞、韓国ドラマと映画鑑賞', '人生は解決すべき問題ではなく、経験すべき現実であることを常に忘れないでください。人生を楽しんでください。常に幸せになることを選んでください！', '4a9033d68ca0261fb16cbfd92650f34b.jpeg', 0),
(20, 'Charmaine Aguanza', 'チャ', '4年', '料理', 'あなたが大きな夢を持ち、達成するために、わたしは全力でサポートします！', '584c2f068607a0c02a666f0d8af7ef32.jpeg', 0),
(21, 'Mercedita Cardeno', 'アルシ', '4年', '料理と音楽鑑賞', '生徒が重要であり、すべての瞬間が重要です。したがって、学習をあきらめないでください。', '1d6e3de7166f05e2cbabb630d438d18e.jpeg', 0),
(22, 'Claudine Canong', ' クラウド', '1年', '歌うこと、音楽鑑賞', '勉強をすることは最初は苦しいかもしれませんが、慣れるととても簡単です。英語を学ぶことも一緒です！', '54f497c92fdfa313bd213e70d92c3496.jpeg', 0);

-- --------------------------------------------------------

--
-- Table structure for table `teacher_en`
--

CREATE TABLE `teacher_en` (
  `teacher_en_id` int(11) NOT NULL,
  `full_name_en` varchar(255) NOT NULL,
  `nickname_en` varchar(255) NOT NULL,
  `year_work_en` varchar(255) NOT NULL,
  `interests_en` text NOT NULL,
  `comments_en` text NOT NULL,
  `teacher_photo_en` text NOT NULL,
  `islocked_en` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `teacher_en`
--

INSERT INTO `teacher_en` (`teacher_en_id`, `full_name_en`, `nickname_en`, `year_work_en`, `interests_en`, `comments_en`, `teacher_photo_en`, `islocked_en`) VALUES
(1, 'Karen Mae Deniega', 'Karen', '6 Years', 'Watching movies and dramas and being active', 'Hello everyone! Dont stop learning English. Study with us. I assure you that youll have fun in learning and with perseverance to study, youll definitely achieve your goal!', '765c0d9f6bb0d516555c01205db9229a.jpeg', 0),
(2, 'Clarisse Ann Lopez', 'Claire', '7 Years', 'Reading articles, traveling, shopping and watching movies', 'Young or old, its never too late to learn the language. Book your lessons today! Thats one of the best decisions youll ever make this year.', '898aabe2a15964945cabb2c42903a9b7.jpeg', 0),
(3, 'Ariel Cabillo', 'Yeng', '5 Years', 'Singing, dancing and eating', 'Lets learn English and have fun! Ill be waiting for you!', '8544531bdc8a135ff052930a9ec2ba8b.jpeg', 0),
(4, 'Vanjoe Cabanluna', 'Van Chan', '6 Years', 'Singing, dancing traveling, volunteering to charity work and cooking', 'Study like theres no tomorrow and always push yourself that you can do more. Be the best you can be.', '75d88d36911427df8928f20dde3cfe7f.jpeg', 0),
(5, 'Vincent Vallera', 'Yas', '2 Years', 'Watching horror films, singing where nobody can hear, surfing the net', 'Learn English is fun! Trust me, Im a teacher!', '809d5f8fbe4f061ec14a173e7cff60a2.jpeg', 0),
(6, 'Lady Rosalie Yap', 'Lady', '2 Years', 'Preaching the Word of God, watching anime and Kdrama', 'Learning the English language is a work in progress. Dont expect to become the best in a snap of a finger, its a continuous learning process.', '6670485a19449de5de039a4ad7e073de.jpeg', 0),
(7, 'Mary Joy Letiado', 'Joy-Joy', '2 Years', 'Watching movies, listening to music and playing badminton', 'We teach, we model, we mold your skills. Thrive to learn and have fun with it!', '1c5bdf6ab9cc1571591b8245ccd2cb07.jpeg', 0),
(8, 'Maricel Canete', 'Marz', '4 Years', 'Cooking, sleeping, eating and watching movies', 'Continue dreaming. Continues studying. The result is confidence. Enjoy English and have fun learning it!', '37d741dcbede913fef640a53ef80945f.jpeg', 0),
(9, 'Lito Mahinay', ' Litz', '5 Years', 'working out to stay fit', 'Have fun learning and never give up!', '38be8fbc36809432a49fa6843257a840.jpeg', 0),
(10, 'Ma. Teresa Bacalan', 'Tere', '2 Years', 'Singing, listening to music, watching Kdrama and English movies', 'Always remember that life is not a problem to solve, but a reality to be experienced. So enjoy life and have fun! Life is too short, choose to be happy!', '7006044cfe9f73ab359ed3b85c70ff74.jpeg', 0),
(11, 'Charmaine Aguanza', 'Cha', '4 Years', 'Cooking', 'Dream big and work hard to achieve your goals in life.', '9980e940dde162b452b7338974a91ecd.jpeg', 0),
(12, 'Mercedita Cardeno', 'Arci', '4 Years', 'Cooking and listening to music', 'Every student matters, every moment counts. So never give up on learning.', 'cf8e5986f674de1c3a2493eecb28d25e.jpeg', 0),
(13, 'Claudine Canong', 'Claud', '1 Year', 'Singing and watching movies', 'The roots of education are bitter, but the fruit is sweet. Just like learning the English language, its difficult at first but the fruit of your hard work is the sweetest.', '73c3a49c0d95ff1cce5af5d3de88f6ec.jpeg', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `person`
--
ALTER TABLE `person`
  ADD PRIMARY KEY (`person_id`);

--
-- Indexes for table `reviews`
--
ALTER TABLE `reviews`
  ADD PRIMARY KEY (`review_id`);

--
-- Indexes for table `review_en`
--
ALTER TABLE `review_en`
  ADD PRIMARY KEY (`review_en_id`);

--
-- Indexes for table `teacher`
--
ALTER TABLE `teacher`
  ADD PRIMARY KEY (`teacher_id`);

--
-- Indexes for table `teacher_en`
--
ALTER TABLE `teacher_en`
  ADD PRIMARY KEY (`teacher_en_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `person`
--
ALTER TABLE `person`
  MODIFY `person_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `reviews`
--
ALTER TABLE `reviews`
  MODIFY `review_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `review_en`
--
ALTER TABLE `review_en`
  MODIFY `review_en_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `teacher`
--
ALTER TABLE `teacher`
  MODIFY `teacher_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `teacher_en`
--
ALTER TABLE `teacher_en`
  MODIFY `teacher_en_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
