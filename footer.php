     <!-- FOOTER -->

     <footer id="footer">

          <div class="container">

               <div class="row">



                    <div class="col-md-4 col-sm-6">

                         <div class="footer-info">

                              <div class="section-title">

                                   <h2>Address</h2>

                              </div>

                              <address>

                                   <p>Unit 714, TGU Tower, Asiatown, I.T. Park,<br> Salinas Drive, Lahug, Cebu City 6000, Philippines</p>

                              </address>



                              <ul class="social-icon">

                                   <li><a href="#" class="fa fa-facebook-square" attr="facebook icon"></a></li>

                                   <li><a href="#" class="fa fa-twitter"></a></li>

                                   <li><a href="#" class="fa fa-instagram"></a></li>

                              </ul>



                              <div class="copyright-text"> 

                                   <p>Copyright &copy; 2021 JICC PH</p>

                                   

                                                                 </div>

                         </div>

                    </div>



                    <div class="col-md-4 col-sm-6">

                         <div class="footer-info">

                              <div class="section-title">

                                   <h2>Contact Info</h2>

                              </div>

                              <address>

                                   <p>+63 32 479 9300</p>

                                   <p><a href="mailto:youremail.co">nils_skype@nilsph.com</a></p>

                              </address>



                              <div class="footer_menu">

                                   <h2>Quick Links</h2>

                                   <ul>

                                        <!-- <li><a href="#">Career</a></li>

                                        <li><a href="#">Investor</a></li> -->

                                        <li><a href="#">Terms & Conditions</a></li>

                                        <!-- <li><a href="#">Refund Policy</a></li> -->

                                   </ul>

                              </div>

                         </div>

                    </div>



                    <div class="col-md-4 col-sm-12">

                         <div class="footer-info newsletter-form">

                              <div class="section-title">

                                   <h2>Map Location</h2>

                              </div>

                              <div>

                                   <div class="form-group">

                                       <!--  <form action="#" method="get">

                                             <input type="email" class="form-control" placeholder="Enter your email" name="email" id="email" required="">

                                             <input type="submit" class="form-control" name="submit" id="form-submit" value="Send me">

                                        </form>

                                        <span><sup>*</sup> Please note - we do not spam your email.</span> -->

                                        <div class="mapouter"><div class="gmap_canvas"><iframe width="415" height="250" id="gmap_canvas" src="https://maps.google.com/maps?q=nils%20cebu&t=k&z=19&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe><a href="https://123movies-to.org">123movies</a><br><style>.mapouter{position:relative;text-align:right;height:265px;width:415px;}</style><a href="https://www.embedgooglemap.net">embed google map iframe</a><style>.gmap_canvas {overflow:hidden;background:none!important;height:265px;width:415px;}</style></div></div>

                                   </div>

                              </div>

                         </div>

                    </div>

                    

               </div>

          </div>

     </footer>