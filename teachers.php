<?php include 'sendtriallesson.php';?>

<!DOCTYPE html>
<html>
<head>
	<title>NILS Online - Our Teachers</title>
	<?php include 'header.php'?>
</head>
<body data-spy="scroll" data-target=".navbar-collapse" data-offset="50">

     <!-- PRE LOADER -->
     <section class="preloader">
          <div class="spinner">

               <span class="spinner-rotate"></span>
               
          </div>
     </section>

      <?php include 'navbar.php';?>

     <!-- STICKY SOCIAL -->
     <?php include 'sticky_social.php';?>
     
     <section>
     		<div class="container">
     			<h1 class="text-center" style="color: #FD6A02;">OUR TEACHERS</h1>
     			<hr>

            <div class="teacher-center">
              <!-- desc1 variable limit 82 characters -->

            <?php

              require_once 'dbconnection.php';              
              $query = $databaseconnection->query("SELECT * FROM teacher WHERE islocked = 0 ORDER BY teacher_id");

              $teacher = [];

              while ($row = $query->fetch_object()) {
                $teacher[] = $row;
              }

              foreach ($teacher as $teacher_details) {
            ?>
     				<div class="responsive" id="teacher-1">
  					  <div class="img-teach">
  					      <img class="img-pop" src="uploads/<?php echo $teacher_details->teacher_photo;?>" alt="Teachers Image">
  					    <div class="desc"><strong>名前:</strong> <?php echo $teacher_details->full_name;?></div>
                <div class="desc"><strong>ニックネーム:</strong> <?php echo $teacher_details->nickname;?></div>
                <div class="desc"><strong>NILS歴:</strong> <?php echo $teacher_details->year_work;?></div>
                <div class="desc"><strong>趣味:</strong> <?php echo $teacher_details->interests;?></div>
                <hr class="hr-design">
                <div  class="desc1" data-city="teacher-1"><?php echo $teacher_details->comments;?></div>
  					  </div>
					 </div>
          <?php
            }
          ?>

<!-- 
					<div class="responsive" id="teacher-2">
					  <div class="img-teach">
					    <img class="img-pop" src="images/teacher-2.jpeg" alt="Forest">
					    <div class="desc"><strong>名前:</strong> Clarisse Ann Lopez</div>
              <div class="desc"><strong>ニックネーム:</strong> クレア</div>
              <div class="desc"><strong>NILS歴:</strong> 7年</div>
              <div class="desc"><strong>趣味:</strong> 雑誌を読む。旅行、買い物、映画鑑賞</div>
              <hr class="hr-design">
              <div class="desc1"  data-city="teacher-2">老若男女問わず、言語を学ぶのに遅いも早いもありません。今すぐやりましょう！きっとそれは今年の中の最高の決断になるでしょう。</div>
					  </div>
					</div>

					<div class="responsive" id="teacher-3">
					  <div class="img-teach">
					    <img class="img-pop" src="images/teacher-3.jpeg" alt="Northern Lights">
					    <div class="desc"><strong>名前:</strong> Ariel Cabillo</div>
              <div class="desc"><strong>ニックネーム:</strong> イェン</div>
              <div class="desc"><strong>NILS歴:</strong> 5年</div>
              <div class="desc"><strong>趣味:</strong> 歌うこと、踊ること、そして食べること！</div>
              <hr class="hr-design">
              <div class="desc1" data-city="teacher-3">を学んで楽しみましょう！お待ちしております！<br><br><br></div>
					  </div>
					</div>

					<div class="responsive" id="teacher-4">
					  <div class="img-teach">
					    <img class="img-pop" src="images/teacher-4.jpeg" alt="Mountains">
					    <div class="desc"><strong>名前:</strong> Vanjoe Cabanluna</div>
              <div class="desc"><strong>ニックネーム:</strong> ヴァンちゃん</div>
              <div class="desc"><strong>NILS歴:</strong> 6年</div>
              <div class="desc"><strong>趣味:</strong> 歌うこと、踊ること、料理、ボランティア活動</div>
              <hr class="hr-design">
              <div class="desc1" data-city="teacher-4">今日1日を最も大事にして勉強して、自分を追い込んでください。できる限りのことをすることであなたの夢が得られるでしょう。</div>
					  </div>
					</div>

					<div class="responsive" id="teacher-5">
					  <div class="img-teach">
					    <img class="img-pop" src="images/teacher-5.jpeg" alt="Mountains">
					    <div class="desc"><strong>名前:</strong> Vincent Vallera</div>
              <div class="desc"><strong>ニックネーム:</strong> ヤス</div>
              <div class="desc"><strong>NILS歴:</strong> 2年</div>
              <div class="desc"><strong>趣味:</strong> ホラー映画を見る、1人で歌うこと、ネットサーフィン</div>
              <hr class="hr-design">
              <div class="desc1">英語を学ぶことは楽しいですよ！私を信じて！なぜならわたしは先生だから！<br><br><br></div>
					  </div>
					</div>

          <div class="responsive" id="teacher-6">
            <div class="img-teach">
              <img class="img-pop" src="images/teacher-6.jpeg">
              <div class="desc"><strong>名前:</strong> Lady Rosalie Yap</div>
              <div class="desc"><strong>ニックネーム:</strong> レディ</div>
              <div class="desc"><strong>NILS歴:</strong> 2年</div>
              <div class="desc"><strong>趣味:</strong> キリスト教の信仰、アニメ・韓国ドラマを見る</div>
              <hr class="hr-design">
              <div class="desc1" data-city="teacher-6">英語を学ぶことは継続することが大切です。すぐにはよくならないです。継続的な学習プロセスを得ましょう。<br><br></div>

            </div>
          </div>

          <div class="responsive">
            <div class="img-teach">
              <img class="img-pop" src="images/teacher-7.jpeg">
             <div class="desc"><strong>名前:</strong> Mary Joy Letiado</div>
              <div class="desc"><strong>ニックネーム:</strong> ジョイ</div>
              <div class="desc"><strong>NILS歴:</strong> 2年</div>
              <div class="desc"><strong>趣味:</strong> 映画鑑賞、音楽鑑賞、バドミントン</div>
              <hr class="hr-design">
              <div class="desc1">わたしたちはあなたのスキルを形作るために、モデル化しあなたに教えます。それを学び、楽しむことを忘れないでください。<br><br></div>

            </div>
          </div>

          <div class="responsive" id="teacher-8">
            <div class="img-teach">
              <img class="img-pop" src="images/teacher-8.jpeg">
              <div class="desc"><strong>名前:</strong> Maricel Canete</div>
              <div class="desc"><strong>ニックネーム:</strong> マーズ</div>
              <div class="desc"><strong>NILS歴:</strong> 4年</div>
              <div class="desc"><strong>趣味:</strong> 料理、睡眠、食事、映画鑑賞</div>
              <hr class="hr-design">
              <div class="desc1" data-city="teacher-8">夢を持ち続けてください。そして達成させるための勉強を続けてください。自身は結果につながります。楽しく学んでください！<br><br></div>

            </div>
          </div>

          <div class="responsive">
            <div class="img-teach">
              <img class="img-pop" src="images/teacher-9.jpeg">
              <div class="desc"><strong>名前:</strong> Lito Mahinay</div>
              <div class="desc"><strong>ニックネーム:</strong> リッツ</div>
              <div class="desc"><strong>NILS歴:</strong> 5年</div>
              <div class="desc"><strong>趣味:</strong> 筋トレ</div>
              <hr class="hr-design">
              <div class="desc1">楽しく学び、決してあきらめないでください！<br><br><br><br></div>

            </div>
          </div>

          <div class="responsive" id="teacher-10">
            <div class="img-teach">
              <img class="img-pop" src="images/teacher-10.jpeg">
              <div class="desc"><strong>名前:</strong> Ma. Teresa Bacalan</div>
              <div class="desc"><strong>ニックネーム:</strong> テレ</div>
              <div class="desc"><strong>NILS歴:</strong> 2年</div>
              <div class="desc"><strong>趣味:</strong> 歌、音楽鑑賞、韓国ドラマと映画鑑賞</div>
              <hr class="hr-design">
              <div class="desc1" data-city="teacher-10">人生は解決すべき問題ではなく、経験すべき現実であることを常に忘れないでください。人生を楽しんでください。常に幸せになることを選んでください！</div> 

            </div>
          </div>

          <div class="responsive">
            <div class="img-teach">
              <img class="img-pop" src="images/teacher-11.jpeg">
              <div class="desc"><strong>名前:</strong> Charmaine Aguanza</div>
              <div class="desc"><strong>ニックネーム:</strong> チャ</div>
              <div class="desc"><strong>NILS歴:</strong> 4年</div>
              <div class="desc"><strong>趣味:</strong> 料理</div>
              <hr class="hr-design">
              <div class="desc1">あなたが大きな夢を持ち、達成するために、わたしは全力でサポートします！<br><br><br><br></div>

            </div>
          </div>

          <div class="responsive">
            <div class="img-teach">
              <img class="img-pop" src="images/teacher-12.jpeg">
              <div class="desc"><strong>名前:</strong> Mercedita Cardeno</div>
              <div class="desc"><strong>ニックネーム:</strong> アルシ</div>
              <div class="desc"><strong>NILS歴:</strong> 4年</div>
              <div class="desc"><strong>趣味:</strong> 料理と音楽鑑賞</div>
              <hr class="hr-design">
              <div class="desc1">すべての生徒が重要であり、すべての瞬間が重要です。したがって、学習をあきらめないでください。<br><br><br></div>

            </div>
          </div>

          <div class="responsive" id="teacher-13">
            <div class="img-teach">
              <img class="img-pop" src="images/teacher-13.jpeg">
              <div class="desc"><strong>名前:</strong> Claudine Canong</div>
              <div class="desc"><strong>ニックネーム:</strong> クラウド</div>
              <div class="desc"><strong>NILS歴:</strong> 1年</div>
              <div class="desc"><strong>趣味:</strong> 歌うこと、音楽鑑賞</div>
              <hr class="hr-design">
              <div class="desc1" data-city="teacher-13">をすることは最初は苦しいかもしれませんが、慣れるととても簡単です。英語を学ぶことも一緒です！</div>

            </div>
          </div> -->

          </div>

					<div class="clearfix"></div>

					<!-- The Modal -->
					<div id="myModal" class="modal-1">
					  <span class="close-1">×</span>
					  <img class="modal-content-1" id="img01">
					  <div id="caption"></div>
            <div id="caption1"></div>
					</div>

     				
     			
     		</div>	
     </section>


    <!-- TRIAL LESSON MODAL -->
     <?php include 'triallesson_modal.php';?>

     <!-- FOOTER -->
     <?php include 'footer.php'?>

     <!--AJAX -->
     <script type="text/javascript">
          $( "#formid" ).submit(function( event ) {
               event.preventDefault();

                $.ajax({
                  url: 'sendtriallesson',
                  type: 'POST',
                  data:  $('#formid').serialize(),
                  success: function(response) { 
                  if(response == 'Success') {  
                      $('#exampleModal').html("Success");
                      $('#exampleModal').modal('show'); //twitter bootstrap modal  
                  },
               });

               });
     </script>

     <script type="text/javascript">
       function myFunction(teacherdata) {
          let dots = document.querySelector(`.desc1[data-city="${teacherdata}"] .dots`);
          let moreText = document.querySelector(`.desc1[data-city="${teacherdata}"] .more`);
          let btnText = document.querySelector(`.desc1[data-city="${teacherdata}"] .myBtn`);

          if (dots.style.display === "none") {
            dots.style.display = "inline";
            btnText.innerHTML = "<br>Read more"; 
            moreText.style.display = "none";
          } else {
            dots.style.display = "none";
            btnText.innerHTML = "<br>Read less"; 
            moreText.style.display = "inline";
          }
        }
     </script>

     <!-- JAVASCIPTS && SCRIPTS-->
     <?php include 'link_scripts.php'?>

</body>
</html>