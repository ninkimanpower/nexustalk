<?php include 'sendtriallesson.php';?>

<!DOCTYPE html>
<html lang="en">
<head>
	<title>NILS Online - Pronunciation</title>
	<?php include 'header.php'?>
</head>
<body data-spy="scroll" data-target=".navbar-collapse" data-offset="50">
     <!-- PRE LOADER -->
     <section class="preloader">
          <div class="spinner">

               <span class="spinner-rotate"></span>
               
          </div>
     </section>
     <!-- Navbar -->
     <?php include 'navbar.php';?>

     <!-- STICKY SOCIAL -->
     <?php include 'sticky_social.php';?>

     <!-- PARALLAX EFFECT -->
      <div class="parallax-pronunciation-upper"></div>

     <!-- SECTION START -->
<!--      <section> -->

     	<div class="container">
     		<div class="row">
     			<div class="col-md-12 col-sm-12">
                         <div class="section-title text-center">
                              <h2 class="callan-title" style="margin-top: 5px;">発音<hr style="border: 1px solid black"><small class="smol">単語の正しい発音を学ぶことを強くおすすめします！</small></h2>
                         </div>
                </div>   
     		</div>
     		<div class="row">
     			<p class="callan-text">このクラスの焦点は、学生が発音における舌の正しい位置を学ぶことです。 教師は、第一言語に関連する個々の問題領域の改善に焦点を当てて、母音と子音の生成についての基本的な理解を示します。 強勢、リズム、イントネーションの基本的なパターンを理解します。 音声学から引き出された材料と技術の選択は、さまざまなパターンと意味の意識を高め、刺激します。</p>
     		</div>
     	</div>

      <div style="background-color: #e9ecef;">
      <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <div class="section-title text-center">
                    <h3 class="callan-subtitle">コース条件</h3>
                    <hr style="border: 1px solid black">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md">
              <p class="callan-text-second"><i class="fa fa-check-square-o fa-3x" aria-hidden="true"></i>英語の基礎知識と各種テストでのスコアアップに必要なスキルを身に着けられる。</p>
            </div>
            <div class="col-md">
              <p class="callan-text-second"><i class="fa fa-check-square-o fa-3x" aria-hidden="true"></i>全世代受講可能</p>
            </div>
        </div>
      </div>
      </div>

<!--      </section> -->


     <!-- TRIAL LESSON MODAL -->
     <?php include 'triallesson_modal.php';?>

     <!-- FOOTER -->
     <?php include 'footer.php'?>

     <!--AJAX -->
     <script type="text/javascript">
          $( "#formid" ).submit(function( event ) {
               event.preventDefault();

                $.ajax({
                  url: 'sendtriallesson',
                  type: 'POST',
                  data:  $('#formid').serialize(),
                  success: function(response) { 
                  if(response == 'Success') {  
                      $('#exampleModal').html("Success");
                      $('#exampleModal').modal('show'); //twitter bootstrap modal  
                  },
               });

               });
     </script>

     <!-- JAVASCIPTS && SCRIPTS -->
     <?php include 'link_scripts.php'?>

</body>
</html>